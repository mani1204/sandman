class CreateSettings < ActiveRecord::Migration[5.2]
  def change
    create_table :settings do |t|
      t.string :entity_type
      t.bigint :entity_id
      t.string :namespace
      t.string :key
      t.string :value

      t.timestamps
    end
  end
end
