class CreateOrders < ActiveRecord::Migration[5.2]
  def change
    create_table :orders do |t|
      t.string :status
      t.json :bill
      t.belongs_to :user, foreign_key: true
      t.belongs_to :customer_address, foreign_key: true

      t.timestamps
    end
  end
end
