class AddDetailsToProducts < ActiveRecord::Migration[5.2]
  def change
    add_reference :products, :category, foreign_key: true
    add_column :products, :price, :decimal
    add_reference :products, :brand, foreign_key: true
    add_column :products, :images, :text, array: true, default: []
  end
end
