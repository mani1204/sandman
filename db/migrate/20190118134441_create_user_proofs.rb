class CreateUserProofs < ActiveRecord::Migration[5.2]
  def change
    create_table :user_proofs do |t|
      t.string :type
      t.integer :value
      t.belongs_to :user, foreign_key: true

      t.timestamps
    end
  end
end
