class CreateOrderPayments < ActiveRecord::Migration[5.2]
  def change
    create_table :order_payments do |t|
      t.belongs_to :order, foreign_key: true
      t.string :mode
      t.string :status

      t.timestamps
    end
  end
end
