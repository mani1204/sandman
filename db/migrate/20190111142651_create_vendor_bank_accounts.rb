class CreateVendorBankAccounts < ActiveRecord::Migration[5.2]
  def change
    create_table :vendor_bank_accounts do |t|
      t.string :name
      t.string :holder_name
      t.string :UPI
      t.string :acc_no
      t.string :string
      t.string :ifsc
      t.string :string
      t.belongs_to :vendor_shop, index: {unique: true}, foreign_key: true

      t.timestamps
    end
  end
end
