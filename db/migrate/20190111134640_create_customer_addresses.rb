class CreateCustomerAddresses < ActiveRecord::Migration[5.2]
  def change
    create_table :customer_addresses do |t|
      t.string :line1
      t.string :line2
      t.string :line3
      t.string :city
      t.string :state
      t.string :country
      t.string :landmark
      t.string :pincode
      t.belongs_to :user, index: true, foreign_key: true
      
      t.timestamps
    end
  end
end
