class CreateOrderTransactions < ActiveRecord::Migration[5.2]
  def change
    create_table :order_transactions do |t|
      t.belongs_to :order, foreign_key: true
      t.string :type
      t.text :comments
      t.string :status

      t.timestamps
    end
  end
end
