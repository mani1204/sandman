class CreateCategories < ActiveRecord::Migration[5.2]
  def change
    create_table :categories, comment: "Categories of the Product. The Sub Categories is also saved making this a self referencing table. " do |t|
      t.string :name, null: false											, comment: 'Name of the Category'
      t.belongs_to :category, foreign_key: true	, comment: 'Present, if it is a Sub Category'

      t.timestamps
    end
  end
end
