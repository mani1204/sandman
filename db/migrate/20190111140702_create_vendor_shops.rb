class CreateVendorShops < ActiveRecord::Migration[5.2]
  def change
    create_table :vendor_shops do |t|
      t.belongs_to :vendor, index: true, foreign_key: true
      t.timestamps
    end
  end
end
