class CreateContactUs < ActiveRecord::Migration[5.2]
  def change
    create_table :contact_us do |t|
      t.string :phone
      t.string :name
      t.string :email
      t.text :message

      t.timestamps
      t.index :phone
    end
  end
end
 