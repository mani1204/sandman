class CreateJoinTableUserVendorShop < ActiveRecord::Migration[5.2]
  def change
    create_join_table :vendor_shops, :users do |t|
      # t.index [:vendor_shop_id, :user_id]
      # t.index [:user_id, :vendor_shop_id]
    end
  end
end
