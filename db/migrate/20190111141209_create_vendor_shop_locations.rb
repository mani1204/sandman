class CreateVendorShopLocations < ActiveRecord::Migration[5.2]
  def change
    create_table :vendor_shop_locations do |t|
      t.string :line1
      t.string :line2
      t.string :line3
      t.string :city
      t.string :state
      t.string :country
      t.string :landmark
      t.string :pincode
      t.timestamps
    end
  end
end
