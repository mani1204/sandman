class AddDetailsToVariants < ActiveRecord::Migration[5.2]
	enable_extension 'citext'
	def change
		add_column :variants, :images, :text, array: true, default: []
		add_column :variants, :configuration, :jsonb
		add_index  :variants, :configuration, using: :gin
	end
end
