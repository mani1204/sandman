class CreateOrderLineItems < ActiveRecord::Migration[5.2]
  def change
    create_table :order_line_items do |t|
      t.belongs_to :order, foreign_key: true
      t.belongs_to :variant, foreign_key: true
      t.decimal :price
      t.integer :qty

      t.timestamps
    end
  end
end
