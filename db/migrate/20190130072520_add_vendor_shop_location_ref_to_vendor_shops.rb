class AddVendorShopLocationRefToVendorShops < ActiveRecord::Migration[5.2]
  def change
    add_reference :vendor_shops, :vendor_shop_location, foreign_key: true, index: {unique: true}
  end
end
