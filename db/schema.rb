# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_04_29_100351) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "citext"
  enable_extension "plpgsql"

  create_table "brands", force: :cascade do |t|
    t.string "name"
    t.string "logo"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name"], name: "index_brands_on_name"
  end

  create_table "carts", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "product_id"
    t.integer "qty"
    t.index ["product_id"], name: "index_carts_on_product_id"
    t.index ["user_id"], name: "index_carts_on_user_id"
  end

  create_table "categories", comment: "Categories of the Product. The Sub Categories is also saved making this a self referencing table. ", force: :cascade do |t|
    t.string "name", null: false, comment: "Name of the Category"
    t.bigint "category_id", comment: "Present, if it is a Sub Category"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["category_id"], name: "index_categories_on_category_id"
  end

  create_table "contact_us", force: :cascade do |t|
    t.string "phone"
    t.string "name"
    t.string "email"
    t.text "message"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["phone"], name: "index_contact_us_on_phone"
  end

  create_table "customer_addresses", force: :cascade do |t|
    t.string "line1"
    t.string "line2"
    t.string "line3"
    t.string "city"
    t.string "state"
    t.string "country"
    t.string "landmark"
    t.string "pincode"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_customer_addresses_on_user_id"
  end

  create_table "customer_profiles", force: :cascade do |t|
    t.string "f_name"
    t.string "l_name"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_customer_profiles_on_user_id", unique: true
  end

  create_table "order_line_items", force: :cascade do |t|
    t.bigint "order_id"
    t.bigint "variant_id"
    t.decimal "price"
    t.integer "qty"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["order_id"], name: "index_order_line_items_on_order_id"
    t.index ["variant_id"], name: "index_order_line_items_on_variant_id"
  end

  create_table "order_payments", force: :cascade do |t|
    t.bigint "order_id"
    t.string "mode"
    t.string "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["order_id"], name: "index_order_payments_on_order_id"
  end

  create_table "order_shipments", force: :cascade do |t|
    t.bigint "order_id"
    t.string "mode"
    t.string "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["order_id"], name: "index_order_shipments_on_order_id"
  end

  create_table "order_transactions", force: :cascade do |t|
    t.bigint "order_id"
    t.string "type"
    t.text "comments"
    t.string "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["order_id"], name: "index_order_transactions_on_order_id"
  end

  create_table "orders", force: :cascade do |t|
    t.string "status"
    t.json "bill"
    t.bigint "user_id"
    t.bigint "customer_address_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["customer_address_id"], name: "index_orders_on_customer_address_id"
    t.index ["user_id"], name: "index_orders_on_user_id"
  end

  create_table "products", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "category_id"
    t.decimal "price"
    t.bigint "brand_id"
    t.text "images", default: [], array: true
    t.index ["brand_id"], name: "index_products_on_brand_id"
    t.index ["category_id"], name: "index_products_on_category_id"
  end

  create_table "settings", force: :cascade do |t|
    t.string "entity_type"
    t.bigint "entity_id"
    t.string "namespace"
    t.string "key"
    t.string "value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "user_proofs", force: :cascade do |t|
    t.string "type"
    t.integer "value"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_user_proofs_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "name"
    t.string "country_code"
    t.string "phone"
    t.string "email"
    t.string "encrypted_password", default: ""
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["country_code", "phone"], name: "index_users_on_country_code_and_phone"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  create_table "users_vendor_shops", id: false, force: :cascade do |t|
    t.bigint "vendor_shop_id", null: false
    t.bigint "user_id", null: false
  end

  create_table "variants", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.decimal "price"
    t.bigint "product_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "images", default: [], array: true
    t.jsonb "configuration"
    t.index ["configuration"], name: "index_variants_on_configuration", using: :gin
    t.index ["product_id"], name: "index_variants_on_product_id"
  end

  create_table "vendor_bank_accounts", force: :cascade do |t|
    t.string "name"
    t.string "holder_name"
    t.string "UPI"
    t.string "acc_no"
    t.string "string"
    t.string "ifsc"
    t.bigint "vendor_shop_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["vendor_shop_id"], name: "index_vendor_bank_accounts_on_vendor_shop_id", unique: true
  end

  create_table "vendor_shop_locations", force: :cascade do |t|
    t.string "line1"
    t.string "line2"
    t.string "line3"
    t.string "city"
    t.string "state"
    t.string "country"
    t.string "landmark"
    t.string "pincode"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "vendor_shops", force: :cascade do |t|
    t.bigint "vendor_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "vendor_shop_location_id"
    t.index ["vendor_id"], name: "index_vendor_shops_on_vendor_id"
    t.index ["vendor_shop_location_id"], name: "index_vendor_shops_on_vendor_shop_location_id", unique: true
  end

  create_table "vendors", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_foreign_key "carts", "products"
  add_foreign_key "carts", "users"
  add_foreign_key "categories", "categories"
  add_foreign_key "customer_addresses", "users"
  add_foreign_key "customer_profiles", "users"
  add_foreign_key "order_line_items", "orders"
  add_foreign_key "order_line_items", "variants"
  add_foreign_key "order_payments", "orders"
  add_foreign_key "order_shipments", "orders"
  add_foreign_key "order_transactions", "orders"
  add_foreign_key "orders", "customer_addresses"
  add_foreign_key "orders", "users"
  add_foreign_key "products", "brands"
  add_foreign_key "products", "categories"
  add_foreign_key "user_proofs", "users"
  add_foreign_key "variants", "products"
  add_foreign_key "vendor_bank_accounts", "vendor_shops"
  add_foreign_key "vendor_shops", "vendor_shop_locations"
  add_foreign_key "vendor_shops", "vendors"
end
