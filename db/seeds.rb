addresses = [
	{
		line1: '11/44',
		line2: 'Karpagam Avenue',
		line3: 'RA Puram',
		city: 'Chennai',
		state: 'Tamilnadu',
		country: 'India',
		landmark: 'Near India Cements',
		pincode: '600028'
	},
	{
		line1: '1/4',
		line2: 'Ram Street',
		line3: 'Tambaram',
		city: 'Chennai',
		state: 'Tamilnadu',
		country: 'India',
		landmark: 'Near India Cements',
		pincode: '600060'
	}
]

vendor_shop_location = Vendor::Shop::Location.create!(addresses)

vendors = [
	{
		name: 'tesark',
		description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.',
		vendor_shops: [
			{
				vendor_shop_location_id: 1
			}, 
			{
				vendor_shop_location_id: 2
			}
		]
	}
]

vendor_users = [
	{
		name: 'Ajaiy',
		email: 'ajaiy@tesark.com',
		country_code: '+91',
		phone: '9600957205',
		password: 'sandman',
		password_confirmation: 'sandman'
	},
	{
		name: 'Hari',
		email: 'hari@tqube.com',
		country_code: '+91',
		phone: '7708143134',
		password: 'sandman',
		password_confirmation: 'sandman'
	}
]

for v in vendors do
	vendor = Vendor.new({name: v[:name], description: v[:description]})
	vendor.save!

	v[:vendor_shops].each_with_index do |vs, index|
		vendorShop = vendor.shops.create!({vendor_shop_location_id: vs[:vendor_shop_location_id]})

		vendorShop.users.create!(vendor_users[index])
	end
end


# Products Seeding

categories = [
	{
		name: 'Cement'
	},
	{
		name: 'OPC-53 Grade Cement',
		parent: 'Cement'
	},
	{
		name: 'PPC Cement',
		parent: 'Cement'
	},
	{
		name: 'Sand'
	},
	{
		name: 'Aggregate'
	},
	{
		name: 'TMT Steel Bars'
	},
	{
		name: 'Fe-500 Grade TMT Bars',
		parent: 'TMT Steel Bars'
	},
	{
		name: 'Fe-550 Grade TMT Bars',
		parent: 'TMT Steel Bars'
	},
	{
		name: 'Fe-600 Grade TMT Bars',
		parent: 'TMT Steel Bars'
	},
	{
		name: 'TMT Binding Wire',
		parent: 'TMT Steel Bars'
	},
	{
		name: 'Rebar Cuplers',
		parent: 'TMT Steel Bars'
	},
	{
		name: 'Bricks'
	},
	{
		name: 'Red Bricks',
		parent: 'Bricks'
	},
	{
		name: 'Blocks'
	},
	{
		name: 'Concrete Solid Blocks',
		parent: 'Blocks'
	}
]

categories.each do |c|
	parent = nil
	unless c[:parent].nil?
		parent = Category.where(name: c[:parent]).first
	end

	Category.create!(name: c[:name], category_id: parent ? parent.id : nil)
end

products = [
	{
		name: 'Deccan OPC-53 Grade',
		description: 'Deccan OPC-53 Grade',
		category: 'OPC-53 Grade Cement',
		price: 350,
		brand: 'Deccan Cements Ltd.',
		variants: []
	},
	{
		name: 'Bhavya OPC Cement',
		description: 'Deccan OPC-53 Grade',
		category: 'OPC-53 Grade Cement',
		price: 375,
		brand: 'Bhavya Cements Ltd.',
		variants: []
	},
	{
		name: 'Ambuja OPC Cement',
		description: 'Ambuja OPC Cement',
		category: 'OPC-53 Grade Cement',
		price: 335,
		brand: 'Ambuja Cements',
		variants: []
	},
	{
		name: 'Coromandel King OPC - 53Grade',
		description: 'Coromandel King OPC - 53Grade',
		category: 'OPC-53 Grade Cement',
		price: 375,
		brand: 'India Cements',
		variants: []
	},
	{
		name: 'Sagar OPC - 53Grade',
		description: 'Sagar OPC - 53Grade',
		category: 'OPC-53 Grade Cement',
		price: 375,
		brand: 'Sagar Cements',
		variants: []
	},
	{
		name: 'Sagar PPC Cement',
		description: 'Sagar PPC Cement',
		category: 'PPC Cement',
		price: 375,
		brand: 'Sagar Cements',
		variants: []
	},
	{
		name: 'Parasakti PPC Cement',
		description: 'Parasakti PPC Cement',
		category: 'PPC Cement',
		price: 375,
		brand: 'Parasakti Cements',
		variants: []
	},
	{
		name: 'Parasakti PPC Cement',
		description: 'Parasakti PPC Cement',
		category: 'PPC Cement',
		price: 375,
		brand: 'Parasakti Cements',
		variants: []
	},
	{
		name: 'Anjani PPC Cement',
		description: 'Anjani PPC Cement',
		category: 'PPC Cement',
		price: 375,
		brand: 'Anjani Cements',
		variants: []
	},
	{
		name: 'KCP PPC Cement',
		description: 'KCP PPC Cement',
		category: 'PPC Cement',
		price: 375,
		brand: 'KCP Cements',
		variants: []
	},
	{
		name: 'Penna PPC Cement',
		description: 'Penna PPC Cement',
		category: 'PPC Cement',
		price: 375,
		brand: 'Penna Cements',
		variants: []
	},
	{
		name: 'Pit Sand',
		description: 'Pit Sand',
		category: 'Sand',
		price: 375,
		brand: '',
		variants: []
	},
	{
		name: 'River Sand',
		description: 'River Sand',
		category: 'Sand',
		price: 375,
		brand: '',
		variants: []
	},
	{
		name: 'Aggregate - 40mm',
		description: 'Aggregate - 40mm',
		category: 'Aggregate',
		price: 375,
		brand: '',
		variants: []
	},
	{
		name: 'Aggregate - 12mm',
		description: 'Aggregate - 12mm',
		category: 'Aggregate',
		price: 375,
		brand: '',
		variants: []
	},
	{
		name: 'D Lite CLC Block',
		description: 'D Lite CLC Block',
		category: 'Concrete Solid Blocks',
		price: 375,
		brand: 'D Lite',
		variants: []
	},
	{
		name: 'Rajahmundry Light Weight Red Bricks - 9in x 4in x 3in',
		description: 'Rajahmundry Light Weight Red Bricks - 9in x 4in x 3in',
		category: 'Red Bricks',
		price: 375,
		brand: 'Rajahmundry',
		variants: []
	},
	{
		name: 'Dhanlaxmi TMT Fe-500 Grade',
		description: 'Dhanlaxmi TMT Fe-500 Grade',
		category: 'Fe-500 Grade TMT Bars',
		price: 375,
		brand: 'Dhanlaxmi',
		variants: [
			{
				name: '8mm',
				configuration: {
					size: '8mm'
				},
				price: 200
			},
			{
				name: '10mm',
				configuration: {
					size: '10mm'
				},
				price: 200
			},
			{
				name: '12mm',
				configuration: {
					size: '12mm'
				},
				price: 200
			},
			{
				name: '16mm',
				configuration: {
					size: '16mm'
				},
				price: 200
			}
		]
	},
	{
		name: 'Fe-550 Grade 20mm Dhanlaxmi TMT Bar - 20mm',
		description: 'Fe-550 Grade 20mm Dhanlaxmi TMT Bar - 20mm',
		category: 'Fe-550 Grade TMT Bars',
		price: 375,
		brand: 'Dhanlaxmi',
		variants: []
	},
	{
		name: 'AF Star TMT Fe-550 Grade-10mm',
		description: 'AF Star TMT Fe-550 Grade-10mm',
		category: 'Fe-550 Grade TMT Bars',
		price: 375,
		brand: 'AF Star',
		variants: []
	}
]

products.each do |p|
	category = Category.where(name: p[:category]).first
	unless category.nil?
		brand = Brand.where(name: p[:brand]).first_or_create
		product = category.products.create!(name: p[:name], description: p[:description], price: p[:price], brand_id: brand[:id])
		p[:variants].each do |v|
			product.variants.create(name: v[:name], description: v[:description], price: v[:price], configuration: v[:configuration])
		end
	end
end