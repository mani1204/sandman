Rails.application.routes.draw do
  root 'landing#index'

  # Admin Users
  devise_for :users, 
  	path: 'vendors', 
  	path_names: { 
  		sign_in: 'login', 
  		sign_out: 'logout', 
  		password: 'secret', 
  		confirmation: 'verification', 
  		unlock: 'unblock', 
  		registration: 'register', 
  		sign_up: '/' 
  	},
  	controllers: {
  		sessions: 'vendors/sessions',
      registrations: 'vendors/registrations'
  	}

  namespace :customers do
    post '/get-otp', to: 'sessions#get_otp'
    post '/verify-otp', to: 'sessions#verify_otp'
    post '/resend-otp', to: 'sessions#resend_otp'
    get '/sign-out', to: 'sessions#logout'

    scope :dashboard do
      get '/', to: 'dashboard#index'
    end
  end  
  
  namespace :vendors do
    get '/', to: 'dashboard#index'
    get 'my_products', to: 'product#my_products'
    get 'manage_products', to: 'product#manage_products'

    scope 'reset_password' do
      get '/', to: 'reset_password#index'
      post '/', to: 'reset_password#send_otp'
      post '/resend_otp', to: 'reset_password#resend_otp'
      post '/submit_otp', to: 'reset_password#verify_otp'
      post '/submit_password', to: 'reset_password#submit_password'
    end
  end

  namespace :api do
    namespace :v1 do
      get '/products/get', to: 'product#get_products'

      namespace :vendors do
        get '/categories', to: 'general#categories'
        get '/product_list', to: 'general#product_list'
      end

      scope 'contact-us' do
        post '/', to: 'contact_us#save'
      end

      namespace 'commerce' do
        scope :cart do
          get '/', to: 'cart#index'
          post 'create', to: 'cart#create'
          post 'update/:cart_id', to: 'cart#update' 
          delete 'delete/:cart_id', to: 'cart#delete'
        end
      end
    end
  end

  scope :commerce do 
    get 'cart', to: 'commerce#cart'
    get 'checkout', to: 'commerce#checkout'
    get 'wishlist', to: 'commerce#wishlist'
    get 'compare', to: 'commerce#compare'
  end

  scope '/products' do 
    get '/:id', to: 'products#index'
  end

  scope 'contact-us' do
    get '/', to: 'contact_us#index'
  end

  scope 'about-us' do
    get '/', to: 'about_us#index'
  end

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  get "errors/404", to: 'errors#not_found'

  get "errors/500", to: 'errors#internal_server_error'
end