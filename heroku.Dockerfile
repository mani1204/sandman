FROM ruby:2.6.2-alpine

RUN apk add --no-cache --update build-base linux-headers git postgresql-dev nodejs tzdata imagemagick yarn curl

WORKDIR /sandman
ADD Gemfile Gemfile.lock /sandman/
RUN bundle install

ADD . .
RUN yarn install
RUN bundle exec rails assets:precompile