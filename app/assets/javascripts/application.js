// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require rails-ujs
//= require activestorage
//= require_tree .
//= require jquery3
//= require jquery_ujs
//= require popper
//= require bootstrap-sprockets
//= require nivoslider/jquery.nivo.slider
//= require meanmenu/jquery.meanmenu
//= require owlcarousel/owl.carousel.min
//= require wow.min
//= require scrollup/jquery.scrollUp 
//= require main
//= require @fortawesome/fontawesome-pro/js/all


window.REACT_INPUTS_VALIDATION = {
  customErrorMessage: {
    "my-own-locale": {},//structure must follow below
    "en-US": {
      textbox: {
        empty: name => `${name}  should not be blank`,
        invalid: name => `${name} invalid format(custom message)`,
        invalidFormat: name => `${name}  Not a number(custom message)`,
        inBetween: name => min => max => `${name} must be in minimum ${min} and maximum ${max} characters only`,
        lessThan: name => min => `${name} cannot less than ${min} characters only`,
        greaterThan: name => max => `${name} cannot greater than ${max} characters only`,
        lengthEqual: name => length => `${name} length must be ${length} characters only`,
        twoInputsNotEqual: () => `Password not matched`
      },

      textarea: {
        empty: name => `${name}  should not be blank`,
        lessThan: name => min => `${name}'s sholud not less than ${min} characters` ,
        inBetween: name => min => max => `${name} allowed only Minimum ${min} characters and maximum ${max} characters`,
        greaterThan: name => max => `${name} cannot greater than ${max} characters`,
        lengthEqual: name => length => `${name} length must be ${length} characters`,
      }
    }
  }
};


