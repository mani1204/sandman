class Customers::SessionsController < ApplicationController
	include SMS
	def get_otp
		response = SMS::MSG91.get_otp mobile: params[:mobile]
		# render plain: render 
		if(response["type"] == 'success')
			render json: {
				success: true, 
				message: 'Otp Sent'
			}
		else
			render json: {
				success: false,
				message: response["message"].gsub('_', ' ').capitalize + '.'
			}
		end
	end

	def resend_otp
		response = SMS::MSG91.resend_otp mobile: params[:mobile]

		if(response["type"] == 'success')
			render json: {
				success: true, 
				message: 'Otp Re Sent'
			}
		else
			render json: {
				success: false,
				message: response["message"].gsub('_', ' ').capitalize + '.'
			}
		end
	end

	def verify_otp
		response = SMS::MSG91.verify_otp mobile: params[:mobile], otp: params[:otp]
		if(response["type"] == 'success')
			user = User.create_customer(params[:mobile])
			sign_in user

			render json: {
				success: true, 
				message: 'Otp Verification successfull'
			}
		else
			render json: {
				success: false,
				message: response["message"].gsub('_', ' ').capitalize + '.'
			}
		end
	end

	def logout
		sign_out current_user

		render json: {
			success: true, 
			message: 'Logged out successfully'
		}
	end
end