class Vendors::ProductController < ApplicationController
	before_action :authenticate_user!

	def my_products
	end

	def manage_products
	end
end
