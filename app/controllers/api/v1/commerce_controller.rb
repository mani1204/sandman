class Api::V1::CommerceController < ApiController
	def add_to_cart
		Commerce::Cart.create(user: current_user, product: Product.find(cart_params[:id]), qty: cart_params[:qty])
		return render json: {
			success: true,
			message: 'Cart Added succesfully'
		}
	end

	def cart
		return render json: {
			success: true,
			cart: Commerce::Cart.where(user: current_user).as_json(include: :product)
		}
	end
end

private 
	def cart_params
		 params.require(:product).permit(:id, :qty)
	end