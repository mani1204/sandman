class Api::V1::Commerce::CartController < ApiController
	before_action :authenticate_user!

	def index
		return render json: {
			success: true,
			cart: Cart.where(user: current_user).as_json(include: :product)
		}
	end

	def create
		cart= Cart.create(user: current_user, product: Product.find(cart_params[:id]), qty: cart_params[:qty])
		return render json: {
			success: true,
			message: 'Cart Added succesfully',
			cart: cart.as_json(include: :product)
		}
	end

	def update
		cart=Cart.find(params[:cart_id])
		cart.update(qty: cart_params[:qty])
		return render json: {
			success: true,
			message: 'Cart updated succesfully',
			cart: cart
		}
	end

	def delete
		cart=Cart.find(params[:cart_id])
		cart.destroy
		return render json: {
			success: true,
			message: 'Product removed succesfully',
			cart: cart
		}
	end
end

private 
	def cart_params
		 params.require(:product).permit(:id, :qty)
	end