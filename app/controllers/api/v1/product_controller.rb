class Api::V1::ProductController < ApiController
  def get_products
  	categories = Category.where(category_id: nil)
  	selected_category_id = params[:category_id].nil? ? categories.first.id : params[:category_id]
  	product_category = Category.where(id: selected_category_id).first
    products = product_category.all_products unless product_category.nil?
  	
  	render json: {
  		categories: categories,
  		products: products,
  		selected_category_id: selected_category_id
  	}
  end
end
