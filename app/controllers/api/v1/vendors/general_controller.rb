class Api::V1::Vendors::GeneralController < ApiController
  before_action :authenticate_user!
  def categories
  	render json: {
  		categories: Category.all
  	}
  end

  def product_list
  	category = Category.find(params['category_id'])
  	render json: {
  		products: category.products
  	}
  end

  def product
  	product Product.find(params[:product_id])
  	render json: {
  		product: product
  	}
  end
end
