class Api::V1::ContactUsController < ApiController
	def save
		ContactUs.create contactus_params
		render json:{success:true, message:"Form submitted successfully."}
	end
end



private 
	def contactus_params
		 params.require(:contact_us).permit(:name, :email, :phone, :message)
	end