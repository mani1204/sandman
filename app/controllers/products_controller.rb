class ProductsController < ApplicationController
	def index
		@product = Product.includes(:variants).find(params[:id])
		@cart = Cart.where({user: current_user,product: @product}).first
	end
end
