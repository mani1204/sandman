# frozen_string_literal: true

class Users::SessionsController < Devise::SessionsController
  # before_action :configure_sign_in_params, only: [:create]

  # GET /resource/sign_in
  def new
    super
  end

  # POST /resource/sign_in
  def create
    super
  end

  # DELETE /resource/sign_out
  # def destroy
  #   super
  # end

  protected

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_sign_in_params
  #   devise_parameter_sanitizer.permit(:sign_in, keys: [:attribute])
  # end
    def after_sign_in_path_for resource
      if resource.shops.first #This user is a vendor, since the model(User) belongs to the vendor shop(Vendor::Shop)
        '/vendors'
      else
        sign_out resource
        '/'
      end
    end

    def after_sign_out_path_for resource
      puts resource
      '/vendor/login'
      # if resource.shops.first #This user is a vendor, since the model(User) belongs to the vendor shop(Vendor::Shop)
      #   '/vendor/login'
      # else
      #   '/'
      # end
    end
end
