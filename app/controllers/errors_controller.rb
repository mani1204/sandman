class ErrorsController < ApplicationController
	def not_found
		return render "404"
	end
	
	def internal_server_error
		return render "500"
	end
end
