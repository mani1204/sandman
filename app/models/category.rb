class Category < ApplicationRecord
  belongs_to :category, optional: true
  has_many :categories
  has_many :products

  def all_products
  	products = []

  	self.categories.includes(:products).each do |c|
  		products << c.products
  	end

  	products << self.products if self.products.count > 0

  	return products
  end
end
