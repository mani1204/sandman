class Vendor::Shop::Location < ApplicationRecord
	has_one :shop, foreign_key: 'vendor_shop_location_id'
end
