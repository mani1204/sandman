class Vendor::Shop < ApplicationRecord
	belongs_to :vendor
	belongs_to :location, foreign_key: 'vendor_shop_location_id'
	has_and_belongs_to_many :users, foreign_key: 'vendor_shop_id', association_foreign_key: 'user_id'
end
