class Product < ApplicationRecord
	has_many :variants
	belongs_to :brand
	belongs_to :category
	has_many :carts, class_name: 'Commerce::Cart', foreign_key: 'product_id'
end
