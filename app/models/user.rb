class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  	devise :database_authenticatable, :registerable,
         :recoverable, :rememberable

    has_and_belongs_to_many :shops, class_name: 'Vendor::Shop', foreign_key: 'user_id', association_foreign_key: 'vendor_shop_id' 
    has_one :customer_profile, class_name: 'Customer::Profile', foreign_key: 'user_id'
    has_many :carts, class_name: 'Commerce::Cart', foreign_key: 'user_id'

    def self.create_customer(mobile)
    	user = User.where(phone: mobile).first
		user = User.create!(country_code: "+91", phone: mobile) if user.nil?
		user.create_customer_profile() if user.customer_profile.nil?

		return user
    end
end
