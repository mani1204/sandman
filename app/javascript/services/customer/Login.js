import axios from '../common/Request'

export default class Login
{
	static getOtp(params)
	{
		let url = '/customers/get-otp'
		return axios.post(url, {
			...params
		})
	}

	static verifyOtp(params)
	{
		let url = '/customers/verify-otp'
		return axios.post(url, {
				...params
			})
	}

	static resendOtp(params)
	{
		let url = '/customers/resend-otp'
		return axios.post(url, {
				...params
			})
	}
}