import axios from '../common/Request'

export default class Login
{
	static postLogin(params)
	{
		let url = '/vendor/login'
		return axios.post(url, {
			params: params
		})
	}
}