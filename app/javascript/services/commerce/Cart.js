import axios from '../common/Request'

export default class Cart
{
	constructor(user = null){
		this.user = user
	}	
	list = (params={}) =>  {
		if(this.user)
		{
			let url = '/api/v1/commerce/cart'
			return axios.get(url, {
				...params
			})
		}
		else
		{
			let cart = JSON.parse(localStorage.getItem('cart'));
			return new Promise((resolve, reject) => {
				resolve({
					data: {
						success: true,
						cart: cart || [],
						message: 'Product listed succesfully'
					}
				})
			})
		}
	}

	create = (params) => {
		if(this.user) //API
		{
			let url = '/api/v1/commerce/cart/create'
			return axios.post(url, {
				...params
			}).then((r) => {
				this.addToLocalStorage(r.data.cart)
				return r
			})
		}
		else //Localstorage
		{
			const product = params.product
			const cart = {
						id: this.generateCartId(cart), //Decide ID
						product_id: product.id,
						qty: product.qty,
						product: product
					}			
			this.addToLocalStorage(cart)
			return new Promise((resolve, reject) => {
				resolve({
					data: {
						success: true, 
						cart: cart,
						message: 'Product Added succesfully'
					}
				})
			})
		}
	}

	update = (cartId,params) => {
		if(this.user) //API
		{
			let url = '/api/v1/commerce/cart/update/' + cartId
			return axios.post(url, {
				...params
			})
			.then((r) => {
				this.updateLocalStorage(r.data.cart,cartId)
				return r
			})
		}
		else 
		{	
			const product = params.product
			const cart = {
						id: cartId,
						product_id: product.id,
						qty: product.qty,
						product: product
					}		
			this.updateLocalStorage(cart,cartId)
			return new Promise((resolve, reject) => {
				resolve({
					data: {
						success: true, 
						cart: cart,
						message: 'Product Updated succesfully'
					}
				})
			})
		}
	}

	delete = (cartId) => {
		if(this.user)
		{
			let url = '/api/v1/commerce/cart/delete/'+ cartId
			this.deleteFromLocalStorage(cartId)
			return axios.delete(url)
		}
		else 
		{
			this.deleteFromLocalStorage(cartId)
			return new Promise((resolve, reject) => {
				resolve({
					data: {
						success: true, 
						cart: [],
						message: 'Product Deleted succesfully'
					}
				})
			})
		}	
	}

	find = (productId) =>{
		let cart = this.findProductFromLocalStorage(productId)
		return new Promise((resolve, reject) => {
			resolve({
				data:{
					success:true,
					cart: cart,
				}
			})
		})
	}

	//Add To LocalStorage
	addToLocalStorage = (c) => {
		let localStorageCart = localStorage.getItem('cart')
		let cart = (localStorageCart == null ? [] : JSON.parse(localStorageCart))
		cart.push(c)
		localStorage.setItem('cart', JSON.stringify(cart))
	}

	generateCartId = () => {
		return Math.random().toString(16).substr(2,1);
	}

	//Remove From LocalStorage
	deleteFromLocalStorage = (cartId) => {
		let localStorageCart = localStorage.getItem('cart');
		let cart = JSON.parse(localStorageCart) || {}; 
		let items = cart || []; 
		cart=items.filter((item)=> item.id !==cartId)
		localStorage.setItem("cart", JSON.stringify(cart)); 
	}

	//Update LocalStorage
	updateLocalStorage = (c,id) => {
		this.deleteFromLocalStorage(id)
		this.addToLocalStorage(c)
	}

	findProductFromLocalStorage = (productID) => {
		let cart = JSON.parse(localStorage.getItem('cart'))
		if(cart){
			let product = cart.filter(c => c.product_id == productID)
			return product.length > 0 ? product[0] : null
		}
		else{
			return null
		}
	}
}
