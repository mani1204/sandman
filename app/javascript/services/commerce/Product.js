import axios from '../common/Request'

export default class Product
{
	static getProducts(params)
	{
		let url = '/api/v1/products/get'
		return axios.get(url, {
			params: params
		})
	}
}