import axios from 'axios';

const csrfToken = document.querySelector('[name="csrf-token"]').content;
const instance = axios.create({
  responseType: 'json',
  headers: {
    'Content-Type': 'application/json',
    'X-CSRF-Token': csrfToken
  }
});

export default instance;