// usage
// first parameter is field alias/name -- unique,required
// second param is field value  -- required
// third param is custom class that has to applied to error label --optional
// fourth param is error message object {validation_rule1: error_message1, validation_rule2: error_message2, default: generic message  } -- optional
// fifth param is wrapper class properties and error class {id: 'wrapper element id', className: 'string of classes'} -- optional
import React from 'react';
import {isDuplicate} from 'utils/validator'

class FormValidator{
  constructor(customRules = {}){
    this.fields = {};
    this.errorMessages = {};
    this.messagesShown = false;
    this.rules = {
      // accepted       : {message: I18n.t('validator.generic.accepted'),       rule: (val) => val === true },
      // alpha          : {message: I18n.t('validator.generic.alpha'),          rule: (val) => this._testRegex(val,/^[A-Z]*$/i) },
      // alpha_num      : {message: I18n.t('validator.generic.alpha'),          rule: (val) => this._testRegex(val,/^[A-Z0-9]*$/i) },
      // alpha_num_dash : {message: I18n.t('validator.generic.alpha_num_dash'), rule: (val) => this._testRegex(val,/^[A-Z0-9_-]*$/i) },
      // card_exp       : {message: I18n.t('validator.generic.card_exp'),       rule: (val) => this._testRegex(val,/^(([0]?[1-9]{1})|([1]{1}[0-2]{1}))\s?\/\s?(\d{2}|\d{4})$/) },
      // card_num       : {message: I18n.t('validator.generic.card_num'),       rule: (val) => this._testRegex(val,/^\d{4}\s?\d{4,6}\s?\d{4,5}\s?\d{0,8}$/) },
      // email          : {message: I18n.t('validator.generic.email'),          rule: (val) => this._testRegex(val,/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i) },
      // gt             : {message: I18n.t('validator.generic.gt'),             rule: (val, options) => this._testRegex(val,/^\d+.?\d*$/) ? parseFloat(val) > parseFloat(options[0]) : false, messageReplace: (message, options) => message.replace(':gt', options[0]) },
      // gte            : {message: I18n.t('validator.generic.gte'),            rule: (val, options) => this._testRegex(val,/^\d+.?\d*$/) ? parseFloat(val) >= parseFloat(options[0]) : false, messageReplace: (message, options) => message.replace(':gte', options[0]) },
      // in             : {message: I18n.t('validator.generic.in'),             rule: (val, options) => options.indexOf(val) > -1, messageReplace: (message, options) => message.replace(':values', this._toSentence(options)) },
      // integer        : {message: I18n.t('validator.generic.integer'),        rule: (val) => this._testRegex(val,/^\d+$/)},
      // lt             : {message: I18n.t('validator.generic.lt'),             rule: (val, options) => this._testRegex(val,/^\d+.?\d*$/) ? parseFloat(val) < parseFloat(options[0]) : false, messageReplace: (message, options) => message.replace(':lt', options[0]) },
      // lte            : {message: I18n.t('validator.generic.lte'),            rule: (val, options) => this._testRegex(val,/^\d+.?\d*$/) ? parseFloat(val) <= parseFloat(options[0]) : false, messageReplace: (message, options) => message.replace(':lte', options[0]) },
      // max            : {message: I18n.t('validator.generic.max'),            rule: (val, options) => val.length <= options[0], messageReplace: (message, options) => message.replace(':max', options[0]) },
      // min            : {message: I18n.t('validator.generic.min'),            rule: (val, options) => val.length >= options[0], messageReplace: (message, options) => message.replace(':min', options[0]) },
      // not_in         : {message: I18n.t('validator.generic.not_in'),         rule: (val, options) => options.indexOf(val) === -1, messageReplace: (message, options) => message.replace(':values', this._toSentence(options)) },
      // numeric        : {message: I18n.t('validator.generic.numeric'),        rule: (val) => this._testRegex(val,/^\d+.?\d*$/)},
      phone          : {message: 'Invalid Phone Number',          rule: (val) => this._testRegex(val,/(\+?\d{0,4})?\s?-?\s?(\(?\d{3}\)?)\s?-?\s?(\(?\d{3}\)?)\s?-?\s?(\(?\d{4}\)?)/)},
      required       : {message: 'This field is required',       rule: (val) => this._testRegex(val,/.+/) },
      // url            : {message: I18n.t('validator.generic.url'),            rule: (val) => this._testRegex(val,/^(https?|ftp):\/\/(-\.)?([^\s/?\.#-]+\.?)+(\/[^\s]*)?$/i) },
      // empty_array    : {message: I18n.t('validator.generic.empty_array'),    rule: (val) => val.length ? true : false },
      // exact          : {message: I18n.t('validator.generic.exact'),          rule: (val, options) => val === options[0], messageReplace: (message, options) => message.replace(':exact', options[0])},
      // postal_code    : {message: I18n.t('validator.generic.postal_code'),    rule: (val) => this._testRegex(val,/^(\s*|\d{5})$/) },
      // duration       : {message: I18n.t('validator.generic.duration'),       rule: (val) => this._testRegex(val,/^([01]?[0-9]|2[0-3])(:[0-5][0-9]|.[0-9])$/) },
      ...customRules,
    };
  }

  getErrorMessages() {
    return this.errorMessages;
  }

  showMessages(){
    this.messagesShown = true;
  }

  hideMessages(){
    this.messagesShown = false;
  }

  // return true if all fields cleared, false if there is a validation error
  allValid(){
    for (var key in this.fields) {
      if( this.fieldValid(key) === false ) {
        return false;
      }
    }
    return true;
  }

  // return true if the one field passed in is valid, false if there is an error
  fieldValid(field){
    return this.fields.hasOwnProperty(field) && this.fields[field] === true;
  }

  // if a message is present, generate a validation error react element
  customMessage(message, customClass){
    if( message && this.messagesShown){
      return this._reactErrorElement(message, customClass);
    }
  }

  message(field, value, testString, customClass, customErrors = {}, wrapperErrorClass = {}){
    this.errorMessages[field] = null;
    this.fields[field] = true;
    var tests = testString.split('|');
    var rule, options, message;
    for(var i = 0; i < tests.length; i++){
      // if the validation does not pass the test
      value = this._valueOrEmptyString(value);
      rule = this._getRule(tests[i]);
      options = this._getOptions(tests[i]);
      // test if the value passes validation
      if(this.rules[rule].rule(value, options) === false){
        this.fields[field] = false;
        if(this.messagesShown){
            message = customErrors[rule] ||
                      customErrors.default ||
                      this.rules[rule].message.replace(':attribute', field.replace(/_/g, ' '));

          this.errorMessages[field] = message;
          this._addWrapperClass(wrapperErrorClass);
          if(options.length > 0 && this.rules[rule].hasOwnProperty('messageReplace')){
            return this._reactErrorElement(this.rules[rule].messageReplace(message, options), customClass);
          } else {
            return this._reactErrorElement(message, customClass);
          }
        }
      }else{
      	this._removeWrapperClass(wrapperErrorClass);
      }
    }
  }

  unique(field, key, obj, arr, customClass, errorMessage, wrapperErrorClass = {}){
    if(isDuplicate(key, obj, arr)){
      this.fields[field] = false;
      this._addWrapperClass(wrapperErrorClass);
      return this._reactErrorElement(errorMessage, customClass);
    }else{
      this.fields[field] =true;
      this._removeWrapperClass(wrapperErrorClass);
    }
  }

  reset(){
    this.fields = {};
  }

  // Private Methods
  _getRule(type){
    return type.split(':')[0];
  }

  _getOptions(type){
    var parts = type.split(':');
    return parts.length > 1 ? parts[1].split(',') : [];
  }

  _valueOrEmptyString(value){
    return typeof value === 'undefined' || value === null ? '' : value;
  }

  _toSentence(arr){
    return arr.slice(0, -2).join(', ') +
    (arr.slice(0, -2).length ? ', ' : '') +
    arr.slice(-2).join(arr.length > 2 ? ', or ' : ' or ');
  }

  _reactErrorElement(message, customClass){
  	const errorLabel = React.createElement('div', {className: customClass + ' validation-message'}, message);
    return errorLabel
  }

  _testRegex(value, regex){
    return value.toString().match(regex) !== null;
  }

  _addWrapperClass(wrapperErrorClass){
  	if(wrapperErrorClass.id && wrapperErrorClass.className){
  		document.getElementById(wrapperErrorClass.id) && document.getElementById(wrapperErrorClass.id).classList.add(wrapperErrorClass.className)
  	}
  }

  _removeWrapperClass(wrapperErrorClass){
  	if(wrapperErrorClass.id && wrapperErrorClass.className){
  		document.getElementById(wrapperErrorClass.id) && document.getElementById(wrapperErrorClass.id).classList.remove(wrapperErrorClass.className)
  	}
  }
}

export default FormValidator
