import { isValid as isValidEmail } from './emailUtils';
import {filter as _collection_filter, find as _collection_find} from 'lodash/collection'

export const isEmpty = (key, obj) => {
	return obj[key] ? "" :  I18n.t('form.error_messages.'+key)
}

export const isDuplicate = (key, obj, arr) => {
	if(obj.id){
		let result = _collection_find(arr, {[key]: obj[key]})
		return result && result.id != obj.id
	}else{
		let results = _collection_filter(arr, {[key]: obj[key]})
		return (results.length > 1)
	}
}