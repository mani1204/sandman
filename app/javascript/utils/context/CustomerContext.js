import React from 'react';

export const CustomerContext = React.createContext({});

export default CustomerContext;