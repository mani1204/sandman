import React from 'react';

export const ProductContext = React.createContext({});

export default ProductContext;