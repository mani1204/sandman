import React from "react"
import PropTypes from "prop-types"
import Header from "components/common/Header"
import Footer from "components/common/Footer"
import BrandSection from "components/partials/landing/BrandSection"
import CustomerContext from "utils/context/CustomerContext"
import Navigation from "components/common/Navigation"
import About from "components/partials/customer/About"
import DashboardContainer from "components/partials/customer/Dashboard"

class Dashboard extends React.Component {

    componentDidMount()
    {
        $.scrollUp({
            scrollName: 'scrollUp',
            topDistance: '550',
            topSpeed: 1000,
            animation: 'fade',
            scrollSpeed: 900,
            animationInSpeed: 1000,
            animationOutSpeed: 1000,
            scrollText: '<i class="far fa-angle-up fa-lg"></i>',
            activeOverlay: false
        });
    }

    render () {
        return (
            <CustomerContext.Provider value={this.props.currentUser}>
              	<div className="wrapper homepage">
                    <Header/>
                    <Navigation> 
                        <ul>
                            <li><a href="index.html">Home</a></li>
                            <li className="active"><a href="account.html">Account</a></li>
                        </ul>
                    </Navigation>
                    <About />
                    <DashboardContainer />
                    <BrandSection/>
                  	<Footer/>
              	</div>
            </CustomerContext.Provider>
        );
    }
}

Dashboard.propTypes = {
  currentUser: PropTypes.object
};

export default Dashboard
