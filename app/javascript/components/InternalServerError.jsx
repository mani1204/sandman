import React from "react"
import PropTypes from "prop-types"
import Header from "components/common/Header"
import Footer from "components/common/Footer"
import Error500 from "components/partials/errors/Error500"
import Navigation from "components/common/Navigation"
import BrandSection from "components/partials/landing/BrandSection"
import CustomerContext from "utils/context/CustomerContext"

class InternalServerError extends React.Component {

    componentDidMount()
    {
        $.scrollUp({
            scrollName: 'scrollUp', // Element ID
            topDistance: '550', // Distance from top before showing element (px)
            topSpeed: 1000, // Speed back to top (ms)
            animation: 'fade', // Fade, slide, none
            scrollSpeed: 900,
            animationInSpeed: 1000, // Animation in speed (ms)
            animationOutSpeed: 1000, // Animation out speed (ms)
            scrollText: '<i class="far fa-angle-up fa-lg"></i>', // Text for element
            activeOverlay: false // Set CSS color to display scrollUp active point, e.g '#00FFFF'
        });
    }

    render () {
        return (
            <CustomerContext.Provider value={this.props.currentUser}>
              	<div className="wrapper homepage">
                    <Header/>
                    <Navigation> 
                        <ul>
                            <li><a href="index.html">Home</a></li>
                            <li className="active"><a href="account.html">500 page</a></li>
                        </ul>
                    </Navigation>
                    <Error500 />
                    <BrandSection/>
              		<Footer/>
              	</div>
            </CustomerContext.Provider>
        );
    }
}
InternalServerError.propTypes = {
  currentUser: PropTypes.object
};

export default InternalServerError
