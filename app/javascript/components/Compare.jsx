import React from "react"
import Header from "./common/Header"
import Footer from "./common/Footer"
import Navigation from './common/Navigation'
import Index from './partials/compare/Index'
class Compare extends React.Component {
	render() {
		return (
			<React.Fragment>
				<div className="wrapper homepage">
					<Header />
					<Navigation>
						<ul>
							<li><a href="/">Home</a></li>
							<li className="active"><a href="#">Compare</a></li>
						</ul>
					</Navigation>
					<Index/>
					<Footer/>
				</div>
			</React.Fragment>
		)
	}
}
export default Compare