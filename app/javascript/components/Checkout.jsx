import React from "react"
import Header from "./common/Header"
import Footer from "./common/Footer"
import Navigation from './common/Navigation'
import CheckoutIndex from './partials/checkout/Index'
import BrandSection from "components/partials/landing/BrandSection"
import PropTypes from "prop-types"
import CustomerContext from "utils/context/CustomerContext"

class Checkout extends React.Component {
	render() {
		return (
			<CustomerContext.Provider value={{user: this.props.currentUser}}>
				<React.Fragment>
					<div className="wrapper homepage">
						<Header />
						<Navigation>
							<ul>
								<li><a href="index.html">Home</a></li>
								<li className="active"><a href="#">Checkout</a></li>
							</ul>
						</Navigation>
						<CheckoutIndex />
						<BrandSection/>
						<Footer />
					</div>
				</React.Fragment>
			</CustomerContext.Provider>
		)
	}
}
Checkout.propTypes = {
    currentUser: PropTypes.object
};
export default Checkout