import React from "react"
import PropTypes from "prop-types"
import Header from "./common/Header"
import Footer from "./common/Footer"
import Slider from "./partials/landing/Slider"
import Banner from "./partials/landing/Banner"
import ProductSection from "./partials/landing/ProductSection"
import CompanyPolicy from "./partials/landing/CompanyPolicy"
import BrandSection from "./partials/landing/BrandSection"
import CustomerContext from "utils/context/CustomerContext"

class Landing extends React.Component {
    componentDidMount()
    {
        $.scrollUp({
            scrollName: 'scrollUp', // Element ID
            topDistance: '550', // Distance from top before showing element (px)
            topSpeed: 1000, // Speed back to top (ms)
            animation: 'fade', // Fade, slide, none
            scrollSpeed: 900,
            animationInSpeed: 1000, // Animation in speed (ms)
            animationOutSpeed: 1000, // Animation out speed (ms)
            scrollText: '<i class="far fa-angle-up fa-lg"></i>', // Text for element
            activeOverlay: false // Set CSS color to display scrollUp active point, e.g '#00FFFF'
        });
    }

    render () {
        return (
            <CustomerContext.Provider value={ {...{user: this.props.currentUser}} }>
              	<div className="wrapper homepage">
                    <Header/>
                    <Slider/>
                    <Banner outerStyle={{marginTop: '30px'}}/>
                    <ProductSection/>
                    <CompanyPolicy/>
                    <BrandSection/>
              		<Footer/>
              	</div>
            </CustomerContext.Provider>
        );
    }
}
Landing.propTypes = {
  currentUser: PropTypes.object
};

export default Landing
