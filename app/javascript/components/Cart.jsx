import React from "react"
import Header from "./common/Header"
import Footer from "./common/Footer"
import Navigation from './common/Navigation'
import CartIndex from './partials/cart/Index'
import PropTypes from "prop-types"
import BrandSection from "components/partials/landing/BrandSection"
import CustomerContext from "utils/context/CustomerContext"

class Cart extends React.Component {
	constructor(props) {
		super(props)
		this.cartRef = React.createRef();
	}
	handleCartDelete = () => {
		this.cartRef.current.fetchCartDetails()
	}
	handleCartInputChange = () => {
		this.cartRef.current.fetchCartDetails()
	}
	render() {
		return (
			<CustomerContext.Provider value={ {...{user: this.props.currentUser}} }>
				<React.Fragment>
					<div className="wrapper homepage">
						<Header ref={this.cartRef} />
						<Navigation>
							<ul>
								<li><a href="/">Home</a></li>
								<li className="active"><a href="#">Cart</a></li>
							</ul>
						</Navigation>
						<CartIndex onCartInputChange={this.handleCartInputChange} onCartDelete={this.handleCartDelete} />
						<BrandSection />
						<Footer />
					</div>
				</React.Fragment>
			</CustomerContext.Provider>				
		)
	}
}
Cart.propTypes = {
	currentUser: PropTypes.object
};
export default Cart