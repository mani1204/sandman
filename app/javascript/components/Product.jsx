import React from "react"
import PropTypes from "prop-types"
import Header from "./common/Header"
import Footer from "./common/Footer"
import Slider from "./partials/landing/Slider"
import Banner from "./partials/landing/Banner"
import CompanyPolicy from "./partials/landing/CompanyPolicy"
import BrandSection from "./partials/landing/BrandSection"
import ProductContext from "utils/context/ProductContext"
import ProductSection from './partials/product/Index'
import CartService from 'services/commerce/Cart'
import swal from 'sweetalert';
import CustomerContext from "utils/context/CustomerContext"
class Product extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            product: props.product,
            cart: props.cart,
            qty: props.cart ? props.cart.qty : 1,
            onQuantityChange: this.handleQuantityChange,
            onAddToCartClick: this.handleAddToCart,
            onUpdateToCartClick: this.handleUpdateToCart,
            onDeleteFromCartClick: this.handleDeleteFromCart
        }
        this.ref = React.createRef();
    }

    componentDidMount(){
        $.scrollUp({
            scrollName: 'scrollUp',
            topDistance: '550',
            topSpeed: 1000,
            animation: 'fade',
            scrollSpeed: 900,
            animationInSpeed: 1000,
            animationOutSpeed: 1000,
            scrollText: '<i class="far fa-angle-up fa-lg"></i>',
            activeOverlay: false
        });

        this.cartService = new CartService(this.props.currentUser)
        if(this.props.currentUser)
        {
            this.setState({cart: this.props.cart})
        }
        else
        {   
            this.cartService.find(this.state.product.id)
            .then(r => {
                this.setState({cart: r.data.cart, qty: r.data.cart ? r.data.cart.qty : 1})
            })
            .catch(e =>{
                swal({
                    title: "Something Wrong",
                    text: "Please Try Again",
                    icon: "error",
                });
            })
        }        
    }

    handleQuantityChange = (e) => {
        this.setState({ qty: e.target.value})
    }

    handleAddToCart = (e) => {
        e.preventDefault()
        this.cartService.create({ product: { ...this.props.product, ...{qty: this.state.qty }} }, {
            headers: {
                'Content-Type': 'application/json',
                'X-CSRF-Token': document.getElementsByName('csrf-token')[0].content
            }
        })
            .then(res => {
                if (res.data.success) {
                    swal({
                        text: "Your Product is Added to the Cart",
                        icon: "success",
                    });
                    this.setState({ cart: res.data.cart })
                    this.ref.current.fetchCartDetails()
                }
                else {
                    swal({
                        title: "Something Wrong",
                        text: "Please Try Again",
                        icon: "error",
                    });
                }
            })
            .catch(e => {
                swal({
                    title: "Something Wrong",
                    text: "Please Try Again",
                    icon: "error",
                });
            })
    }

    handleUpdateToCart = (e) => {
        e.preventDefault()
        this.cartService.update( this.state.cart.id,{product:{ ...this.props.product, ...{qty: this.state.qty }} }, {
            headers: {
                'Content-Type': 'application/json',
                'X-CSRF-Token': document.getElementsByName('csrf-token')[0].content
            }
        })
            .then(res => {
                if (res.data.success) {
                    swal({
                        text: "Your Product is Updated to the Cart",
                        icon: "success",
                    });
                    this.setState({ cart: res.data.cart, qty: res.data.cart.qty })
                    this.ref.current.fetchCartDetails()
                }
                else {
                    swal({
                        title: "Something Wrong",
                        text: "Please Try Again",
                        icon: "error",
                    });
                }
            })
            .catch(e => {
                swal({
                    title: "Something Wrong",
                    text: "Please Try Again",
                    icon: "error",
                });
            })
    }

    handleDeleteFromCart = (e) => {
        e.preventDefault()  
        this.cartService.delete(this.state.cart.id)
        .then((res) => {
            if (res.data.success) {
                swal({
                        text: "Your Product is Deleted from the Cart",
                        icon: "success",
                    });
                this.setState({ cart: null, qty: 1})
                this.ref.current.fetchCartDetails()
            }
            
        })
        .catch((e) => {
            swal({
                title: "Error!",
                text: "Please Try Again!",
                icon: "error",
                button: "Ok!",
            });
        })
        this.ref.current.fetchCartDetails()
    }

    render() {
        return (
            <CustomerContext.Provider value={{user: this.props.currentUser}}>
                 <ProductContext.Provider value={this.state} >
                  	<div className="wrapper homepage">
                        <Header ref={this.ref}/>
                        <ProductSection/>
                        <CompanyPolicy/>
                        <BrandSection/>
                  		<Footer/>
                  	</div>
                </ProductContext.Provider>
            </CustomerContext.Provider>
        );
    }
}

Product.propTypes = {
    currentUser: PropTypes.object
};
export default Product
