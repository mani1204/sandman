import React from "react"
import PropTypes from "prop-types"
import CallIcon from "packs/images/call.png"
import Logo from "packs/images/logo.png"
import Search from "./Search"
import Cart from "./Cart"
import CustomerLogin from './customer-login/Index'
import CustomerContext from "utils/context/CustomerContext"
import axios from 'axios'
class Header extends React.Component {

	constructor(props)
	{
		super(props)

		this.openCustomerLogin 	= this.openCustomerLogin.bind(this)
		this.closeCustomerLogin = this.closeCustomerLogin.bind(this)

		this.state = {
			showCustomerLogin: false
		}
	}

	componentDidMount()
	{
		jQuery('.mobile-menu nav').meanmenu({
		    meanScreenWidth: "991",
		    meanMenuContainer: "div.mobile-menu"
		});
	}
	
	openCustomerLogin()
	{
		this.setState({
			showCustomerLogin: true
		})
	}

	closeCustomerLogin()
	{
		this.setState({
			showCustomerLogin: false
		})
	}

	customerLogout()
	{
		axios.get('/customers/sign-out').then(function(r){
			location.reload()
		}).catch(function(e){
			location.reload()
		}).then(function(){
			console.log('completed')
		})
	}

  	render () {
  		let customer = this.context
  		let customerDropDown;
  		if(customer && customer.user){
			customerDropDown = (<ul className="ht-dropdown">
	            <li>
	            	<a href="#" onClick={this.customerLogout}>
	            		Logout
	            	</a>
	            </li>
	        </ul>)
		}
		else{
			customerDropDown = (<ul className="ht-dropdown">
	            <li>
	            	<a href="#" onClick={this.openCustomerLogin}>
	            		Customer Login
	            	</a>
	            </li>
	            <li>
	            	<a href="/vendors/login">
	            		Vendor Login
	            	</a>
	            </li>                                         
	        </ul>)
		}

	    return (
	      	<React.Fragment>
	     		<header>
		            <div className="header-top">
		                <div className="container">
		                    <div className="row">
		                        <div className="col-lg-4 col-md-12 d-center">
		                            <div className="header-top-left">
		                                <img src={CallIcon}/>
		                                Call Us : +11 222 3333
		                            </div>                        
		                        </div>
		                        <div className="col-lg-4 col-md-6 ml-auto">
		                        	<Search />                                 
		                        </div>
		                    </div>
		                </div>
		            </div>
		            <div className="header-bottom header-sticky">
		                <div className="container">
		                    <div className="row">
		                        <div className="col-xl-3 col-lg-2 col-sm-5 col-5">
		                            <div className="logo">
		                                <a href="/">
		                                	<img src={Logo} />
		                                </a>
		                            </div>
		                        </div>
		                        <div className="col-xl-6 col-lg-7 d-none d-lg-block">
		                            <div className="middle-menu pull-right">
		                                <nav>
		                                    <ul className="middle-menu-list">
		                                        <li>
		                                        	<a href="/">
		                                        		<span>home </span>
		                                        		<i className="fas fa-user"></i>
		                                        	</a>
		                                        </li>
		                                        <li>
		                                        	<a href="/about-us">
		                                        		about us
		                                        	</a>
		                                        </li>                                        
		                                        <li>
		                                        	<a href="/commerce/cart">
		                                        		<span>shop </span>
		                                        		<i className="fal fa-angle-down fa-lg"></i>
		                                        	</a>
		                                            <ul className="ht-dropdown dropdown-style-two">
		                                                <li><a href="/commerce/cart">Cart</a></li>
		                                                <li><a href="/commerce/checkout">Checkout</a></li>
		                                                <li><a href="/commerce/wishlist">Wishlist</a></li>
		                                            </ul>
		                                        </li>                                        
		                                        <li><a href="/contact-us">contact us</a></li>                                        
		                                    </ul>
		                                </nav>
		                            </div>
		                        </div>
		                        <div className="col-lg-3 col-sm-7 col-7">
		                            <div className="cart-box text-right">
		                                <ul>
		                                    <li>
		                                    	<a href="#">
		                                    		<i className="fas fa-user"></i>
		                                    	</a>
		                                    	{customerDropDown}
		                                    </li>              
		                                    <li>
		                                    	<Cart ref={this.props.innerRef}/>
		                                    </li>
		                                </ul>
		                            </div>
		                        </div>
		                        <div className="col-sm-12 d-lg-none">
		                            <div className="mobile-menu">
		                                <nav>
		                                    <ul>
		                                        <li><a href="">home</a>
		                                        </li>
		                                        <li><a href="/commerce/cart">shop</a>
		                                            <ul>
		                                            	<li><a href="/commerce/cart">Cart</a></li>
		                                                <li><a href="/commerce/checkout">Checkout</a></li>
		                                                <li><a href="/commerce/wishlist">Wishlist</a></li>
		                                            </ul>
		                                        </li>
		                                        <li><a href="/about-us">about us</a></li>
		                                        <li><a href="/contact-us">contact us</a></li>
		                                    </ul>
		                                </nav>
		                            </div>
		                        </div>
		                    </div>
		                </div>
		            </div>
		        </header>
		        <CustomerLogin show={this.state.showCustomerLogin} onClose={this.closeCustomerLogin} />
	      	</React.Fragment>
	    );
  	}
}

Header.contextType = CustomerContext;

export default React.forwardRef((props, ref) => <Header innerRef={ref} {...props}/>);
