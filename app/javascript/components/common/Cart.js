import React from "react"
import image1 from "packs/images/1.jpg"
import CartService from 'services/commerce/Cart'
import CustomerContext from "utils/context/CustomerContext"
class Cart extends React.Component
{
    constructor(props){
        super(props)
        this.state={
            products:[],
            total:0
        } 
    }

    componentDidMount()
    {
        this.cartService = new CartService(this.context.user)
        this.fetchCartDetails()
    }

    fetchCartDetails()
    {
        this.cartService.list()
        .then((r) => {
            this.setState({products:r.data.cart, total: this.getTotal(r.data.cart)})
        })
        .catch((e) => {
            console.log(e)
         })           
    }

    handleRemoveClick(product) 
    {
        this.cartService.delete(product.id)
        .then((r) => {
            if(r.data.success)
            {
                this.fetchCartDetails()
            }
        })
        .catch((e) => {
            console.log(e)
        })
    }

    getTotal(products){
        return products.reduce((total,p)=>{return total + (p.qty * p.product.price)},0)
    }  

     _renderCartProducts(products)
    {
        return(
            products.map((product, index) => (
                <div key={index}>
                    <div className="single-cart-box" >
                        <div className="cart-img">
                            <a href={"../products/"+product.product.id}>
                                <img src={image1}/>
                            </a>
                        </div>
                        <div className="cart-content">
                            <h6>
                                <a href={"../products/"+product.product.id}>{product.product.name}</a>
                            </h6>
                            <span>{product.qty} × {product.product.price}</span>
                        </div>
                        <a className="del-icone" href="#" onClick={()=>this.handleRemoveClick(product)}>
                            <i className="fal fa-window-close"></i>
                        </a>
                    </div>
                </div>
            ))
        )
    }

	render()
	{  
		return (
			<React.Fragment>
				<a href="/commerce/cart">
            		<i className="fa fa-shopping-basket"></i>
            		<span className="cart-counter">{this.state.products.length}</span>
            	</a>
            	<ul className="ht-dropdown main-cart-box">
                    {this.state.products.length<=0 ? <p> No items in the cart </p> :
                        <li>    
                            {this._renderCartProducts(this.state.products)}
                            <div className="cart-footer fix">
                                <h5>Total :<span className="f-right"><i className="fas fa-rupee-sign cart-rupee-symbol mr-1"></i> {this.state.total}</span></h5>
                                <div className="cart-actions">
                                    <a className="checkout" href="../commerce/checkout">Checkout</a>
                                </div>
                            </div>
                        </li>
                    }
                </ul>
			</React.Fragment>
		);
	}
}
Cart.contextType = CustomerContext;

export default Cart