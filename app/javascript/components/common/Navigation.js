import React from 'react';
import ReactDOM from 'react-dom';

class Navigation extends React.Component {
  render(){
    return (
      <div>         
        <div className="breadcrumb-area ptb-60 ptb-sm-30">
          <div className="container">
            <div className="breadcrumb">
              {this.props.children}
            </div>
          </div>
        </div>    
      </div>
    );
  }
}

export default Navigation;