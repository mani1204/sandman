import React from 'react'
import {Container, Row, Col, Form, Button, Alert} from 'react-bootstrap'
import Login from '../../../services/customer/Login'
import FormValidator from 'utils/formValidator';
class OtpForm extends React.Component
{
	constructor(props)
	{
		super(props)

		this.state = {
			form: {
				valid: null,
				success: null,
				error_message: null,
				success_message: null
			}
		}
    	this.handleSubmit = this.handleSubmit.bind(this);
    	this.resendOtp    = this.resendOtp.bind(this)

    	this.validator = new FormValidator();
	}

	handleSubmit(event) {
		event.preventDefault();
		if(this.validator.allValid()){
			this.validator.hideMessages()
			Login.verifyOtp({
				mobile: this.props.mobile,
				otp: 	this.props.otp
			}).then((r) => {
				if(r.data.success)
				{
					this.setState({
						form: {
							...this.state.form,
							valid: true,
							success: true,
							success_message: r.data.message
						}		
					})
					this.props.onCustomerOtpSubmit();
				}
				else
				{
					console.log(r.data.message)
					this.setState({
						form: {
							valid: false,
							success: false,
							error_message: r.data.message
						}		
					})
				}
			}).catch((e) => {
				this.setState({
						form: {
							...this.state.form,
							valid: false,
							success: false,
							error_message: 'Invalid OTP'
						}		
					})
			}).then((r) => {
				console.log('completed')	
			})
		}
		else{
			this.validator.showMessages();
			this.setState({
					form: {
						...this.state.form,
						valid: false,
					}		
				})
		}
	}

	resendOtp(event)
	{
		event.preventDefault()
		this.validator.hideMessages()
		Login.resendOtp({
			mobile: this.props.mobile
		}).then((r) => {
			if(r.data.success == true)
			{
				this.setState({
					form: {
						...this.state.form,
						valid: true,
						success: true,
						success_message: r.data.message
					}		
				})
			}
			else
			{
				this.setState({
					form: {
						valid: false,
						success: false,
						error_message: r.data.message
					}		
				})
			}
		}).catch((e) => {
			this.setState({
					form: {
						...this.state.form,
						valid: false,
						success: false,
						error_message: 'Invalid OTP'
					}		
				})
		}).then((r) => {
			console.log('completed')	
		})
	}

	formAlert()
	{
		if(this.state.form.success == false){
			return (
				<Alert variant={'danger'}>
					{this.state.form.error_message}
				</Alert>
			)
		}
		else if(this.state.form.success == true){
			return (
				<Alert variant={'success'}>
					{this.state.form.success_message}
				</Alert>
			)
		}
	}

	render(){
		return (
			<React.Fragment>
				<Row>
					<Col xs={12}>
						{this.formAlert()}

						<Form.Group>
    						<Form.Control type="text" placeholder="Enter your OTP..." onChange={this.props.onOtpChange} value={this.props.otp}/>
    						{this.validator.message("otp" , this.props.otp, 'required', 'text-danger')}
    						<a href="#" className="float-right" onClick={this.resendOtp}>
    							<u>
    								Resend Otp
    							</u>
    						</a>
    					</Form.Group>
					</Col>
					<Col xs={12}>
						<Button variant="link" className="float-left" style={{marginTop: '20px', paddingLeft: '0px'}} onClick={this.props.goBack}>
							<i className="fas fa-long-arrow-alt-left" style={{marginRight: '3px'}}></i>
							Back
						</Button>
						<Button variant="primary" type="submit" className="float-right customer-btn" onClick={this.handleSubmit}>
							Submit
						</Button>
					</Col>
				</Row>
			</React.Fragment>
		);
	}
}

export default OtpForm