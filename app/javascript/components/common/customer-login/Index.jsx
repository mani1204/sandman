import React from 'react'
import LoginForm from './LoginForm'
import OtpForm from './OtpForm'
import { Modal, Button } from 'react-bootstrap'
class Index extends React.Component
{
	constructor(props)
	{
		super(props)

		this.state = {
			form: 'login',
			mobile: '',
			otp: ''
		}
		
		this.handleCustomerLoginSubmit 	= 	this.handleCustomerLoginSubmit.bind(this)
		this.handleCustomerOtpSubmit 	= 	this.handleCustomerOtpSubmit.bind(this)
		this.handleMobileChange	 		= 	this.handleMobileChange.bind(this)
		this.handleOtpChange 			= 	this.handleOtpChange.bind(this)
		this.goToLoginForm				=	this.goToLoginForm.bind(this)
		this.handleModalClose			=	this.handleModalClose.bind(this)
	}

	handleMobileChange(event) {
		let mobile = event.target.value
		if (mobile.length <= 10)
		{
			this.setState({mobile: mobile});
		}
	}

	handleOtpChange(event) {
		let otp = event.target.value
		if (otp.length <= 10)
		{
			this.setState({otp: otp});
		}
	}

	handleCustomerLoginSubmit()
	{
		this.setState({
				form: 'otp',
				otp: ''
			})
	}

	handleCustomerOtpSubmit()
	{
		window.location = "/"
	}

	goToLoginForm(){
		this.setState({
				'form': 'login'
			})
	}

	handleModalClose(){
		this.setState({
				'form': 'login',
				mobile: '',
				otp: ''
			})
		this.props.onClose()
	}

	formSection()
	{
		if(this.state.form == 'login')
		{
			return <LoginForm onLoginFormSubmit={this.handleCustomerLoginSubmit} onMobileChange={this.handleMobileChange} mobile={this.state.mobile}/>
		}
		else if(this.state.form == 'otp')
		{
			return <OtpForm onCustomerOtpSubmit={this.handleCustomerOtpSubmit} 
							onOtpChange={this.handleOtpChange} 
							mobile={this.state.mobile} 
							otp={this.state.otp} 
							goBack={this.goToLoginForm}
							/>
		}
	}

	render(){
		return (
			<React.Fragment>
				<Modal show={this.props.show} onHide={this.handleModalClose} backdrop="static" centered={true} keyboard={false}>
					<Modal.Header closeButton>
						<Modal.Title>Customer Login</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						{this.formSection()}
					</Modal.Body>
				</Modal>
			</React.Fragment>
		);
	}
}

export default Index