import React from 'react'
import {Container, Row, Col, Form, Button, Alert} from 'react-bootstrap'
import Login from 'services/customer/Login'
import FormValidator from 'utils/formValidator';
class LoginForm extends React.Component
{
	constructor(props)
	{
		super(props)	
		this.state = {
			form: {
				valid: null,
				success: null,
				error_message: null
			}
		}

    	this.handleSubmit = this.handleSubmit.bind(this);
    	this.validator = new FormValidator();
	}

	handleSubmit(event) {
		event.preventDefault();
		if(this.validator.allValid()){
			this.validator.hideMessages()
			Login.getOtp({
				mobile: this.props.mobile
			}).then((r) => {
				if(r.data.success == true){
					this.setState({
						form: {
							...this.state.form,
							valid: true,
							success: true,
						}		
					})
					this.props.onLoginFormSubmit();
				}
				else{
					this.setState({
						form: {
							valid: false,
							success: false,
							error_message: r.data.message
						}		
					})
				}
			}).catch((e) => {
				this.setState({
						form: {
							...this.state.form,
							valid: false,
							success: false,
							error_message: 'Error in sending OTP'
						}		
					})
			}).then((r) => {
				console.log('completed')	
			})
		}
		else{
			this.validator.showMessages();
			this.setState({
					form: {
						...this.state.form,
						valid: false,
					}		
				})
		}
		
	}

	formAlert()
	{
		if(this.state.form.success == false){
			return (
				<Alert variant={'danger'}>
					{this.state.form.error_message}
				</Alert>
			)
		}
	}

	render(){
		return (
			<React.Fragment>
				<Row>
					<Col xs={12}>
						{this.formAlert()}
						
						<Form.Group>
    						<Form.Control type="text" placeholder="Enter your mobile number here..." onChange={this.props.onMobileChange} value={this.props.mobile}/>
							{this.validator.message("mobile" , this.props.mobile, 'required|phone', 'text-danger')}
    					</Form.Group>
					</Col>
					<Col xs={12}>
						<Button variant="primary" type="submit" className="float-right customer-btn" onClick={this.handleSubmit}>
							Get OTP
						</Button>
					</Col>
				</Row>
			</React.Fragment>
		);
	}
}

export default LoginForm