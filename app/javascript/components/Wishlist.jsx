import React from "react"
import Header from "./common/Header"
import Footer from "./common/Footer"
import Navigation from './common/Navigation'
import WishelistIndex from './partials/wishlist/Index'
import BrandSection from "components/partials/landing/BrandSection"
import PropTypes from "prop-types"
import CustomerContext from "utils/context/CustomerContext"

class Wishelist extends React.Component {

	render() {
		return (
			<CustomerContext.Provider value={{user: this.props.currentUser}}>
				<React.Fragment>
					<div className="wrapper homepage">
						<Header />
						<Navigation>
							<ul>
								<li><a href="/">Home</a></li>
								<li className="active"><a href="#">Wishelist</a></li>
							</ul>
						</Navigation>
						<WishelistIndex />
						<BrandSection/>
						<Footer />
					</div>
				</React.Fragment>
			</CustomerContext.Provider>
			
		)
	}
}
Wishelist.propTypes = {
    currentUser: PropTypes.object
};
export default Wishelist