import React from 'react';
import CustomerLogin from 'components/common/customer-login/Index'

class Login extends React.Component{
	constructor(props){
		super(props);
		this.state = {
			showCustomerLoginPopup: false
		}
	}

	openCustomerLoginPopup = () =>
	{
		this.setState({
			showCustomerLoginPopup: true
		})
	}

	closeCustomerLoginPopup = () =>
	{
		this.setState({
			showCustomerLoginPopup: false
		})
	}
	render(){
		return (
			<div className="coupon-area">
				<div className="container">
					<div className="section-title mb-20">
						<h2>Checkout</h2>
					</div>
					<div className="row">
						<div className="col-lg-12">
							<div className="coupon-accordion">
								<h3>Returning customer? <span id="showlogin" onClick={this.openCustomerLoginPopup}>Click here to login</span></h3>
								<div className="inputs">
									<CustomerLogin show={this.state.showCustomerLoginPopup} onClose={this.closeCustomerLoginPopup} />
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		)
	}
}
export default Login