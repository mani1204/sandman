import React from 'react';

class OrderDetails extends React.Component{
	render(){
		return(
			<div className="col-lg-6 col-md-6">
	            <div className="your-order">
	                <h3>Your order</h3>
	                <div className="your-order-table table-responsive">
	                    <table>
	                        <thead>
	                            <tr>
	                                <th className="product-name">Product</th>
	                                <th className="product-total">Total</th>
	                            </tr>
	                        </thead>
	                        <tbody>
	                        	{this.props.products.map((product, index) => (
	                            <tr className="cart_item" key={index}>
	                                <td className="product-name">
	                                    {product.product.name}<strong className="product-quantity"> * {product.qty}</strong>
	                                </td>
	                                <td className="product-total">
	                                    <span className="amount">{(product.product.price)*(product.qty)}</span>
	                                </td>
	                            </tr>
	                           ))} 
	                        </tbody>
	                        <tfoot>
	                            <tr className="cart-subtotal">
	                                <th>Cart Subtotal</th>
	                                <td><span className="amount"><i className="fas fa-rupee-sign cart-rupee-symbol mr-1"></i>{this.props.totalPrice}</span></td>
	                            </tr>
	                            <tr className="order-total">
	                                <th>Order Total</th>
	                                <td><strong><span className="amount"><i className="fas fa-rupee-sign cart-rupee-symbol mr-1"></i>{this.props.totalPrice}</span></strong>
	                                </td>
	                            </tr>	 
	                        </tfoot>
	                    </table>
	                </div>
	                <div className="payment-method">
	                    <div className="payment-accordion">
	                        <div className="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
	                            <div className="panel panel-default">
	                                <div className="panel-heading" role="tab" id="headingOne">
	                                    <h4 className="panel-title">
	                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">Direct Bank Transfer</a>
	                                    </h4>
	                                </div>
	                                <div id="collapseOne" className="panel-collapse collapse  in show" role="tabpanel" aria-labelledby="headingOne">
	                                    <div className="panel-body">
	                                        <p>Make your payment directly into our bank account. Please use your Order ID as the payment reference. Your order won’t be shipped until the funds have cleared in our account.</p>
	                                    </div>
	                                </div>
	                            </div>
	                            <div className="panel panel-default">
	                                <div className="panel-heading" role="tab" id="headingTwo">
	                                    <h4 className="panel-title">
	                                        <a className="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">Cheque Payment</a>
	                                    </h4>
	                                </div>
	                                <div id="collapseTwo" className="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
	                                    <div className="panel-body">
	                                        <p>Please send your cheque to Store Name, Store Street, Store Town, Store State / County, Store Postcode.</p>
	                                    </div>
	                                </div>
	                            </div>
	                            <div className="panel panel-default">
	                                <div className="panel-heading" role="tab" id="headingThree">
	                                    <h4 className="panel-title">
	                                        <a className="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">PayPal</a>
	                                    </h4>
	                                </div>
	                                <div id="collapseThree" className="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
	                                    <div className="panel-body">
	                                        <p>Pay via PayPal; you can pay with your credit card if you don’t have a PayPal account.</p>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                        <div className="order-button-payment">
	                            <input type="submit" value="Place order" />
	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div>
		)
	}
}
export default OrderDetails