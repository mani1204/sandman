import React from 'react';
import { Textbox, Select, Textarea } from 'react-inputs-validation'
import validator from 'validator';
import 'react-inputs-validation/lib/react-inputs-validation.css';
import countries from 'utils/countries'
import CustomerContext from "utils/context/CustomerContext"

class BillingDetailsForm extends React.Component{
	constructor(props) {
		super(props)
		this.state = {
			errors:{
				countryError:true,
				firstNameError:true,
				lastNameError:true,
				companyNameError:true,
				streetAddressError:true,
				townError:true,
				countyError:true,
				postcodeError:true,
				emailError:true,
				mobileError:true,
				checkoutMessageError:true,
			},
			validate: false,
		}
    }
	render(){
		return(
			<div className="col-lg-6 col-md-6">
				<div className="checkbox-form">
					<h3>Billing Details</h3>
					<div className="row">
						<div className="col-md-12">
							<div className="country-select mb-30">
								<label>Country <span className="required">*</span></label>
								<Select 
									id = "country"
									name='Country' 
									onBlur={() => {}}
									optionList={countries} 
									customStyleContainer={{padding : '1%' , border: '1px solid #e5e5e5'}}
									value={this.props.form.country}
									onChange={(value) => this.props.handleInputChange('country', value)}
									validate={this.state.validate}
									validationOption={{
										name: 'Country',
										check: true,
										required: true,
									}}
								/>
							</div>
						</div>

						<div className="col-md-6">
							<div className="checkout-form-list">
								<label>First Name <span className="required">*</span></label>
								<Textbox
									id='firstName'
									name="First_Name"
									classNameInput="form-control"
									type="text"
									maxLength={20}
									value = {this.props.form.firstName}
									onChange={(value) => this.props.handleInputChange('firstName', value)}
									validate={this.state.validate}
									onBlur={() => { }}
									validationOption={{
										name: 'First Name',
										check: true,
										required: true,
										customFunc: firstName => {
											if(validator.isAlpha(firstName,["en-US"]))
											{
												return true;
											} 
											else
											{
												return "First Name field contains only alphabets";
											}
										},
										min: 3,
									}}
								/>
							</div>
						</div>

						<div className="col-md-6">
							<div className="checkout-form-list mb-30">
								<label>Last Name <span className="required">*</span></label>
								<Textbox
									id='lastName'
									name="Last_Name"
									classNameInput="form-control"
									type="text"
									maxLength={20}
									value = {this.props.form.lastName}
									onChange={(value) => this.props.handleInputChange('lastName', value)}
									validate={this.state.validate}
									onBlur={() => { }}
									validationOption={{
										name: 'Last Name',
										check: true,
										required: true,
										customFunc: lastName => {
											if(validator.isAlpha(lastName,["en-US"]))
											{
												return true;
											} 
											else
											{
												return "Last Name field contains only alphabets";
											}
										},
										min: 3,
									}}
								/>
							</div>
						</div>

						<div className="col-md-12">
							<div className="checkout-form-list mb-30">
								<label>Company Name</label>
								<Textbox
								id='companyName'
								name="Company_Name"
								classNameInput="form-control"
								type="text"
								maxLength={20}
								value = {this.props.form.companyName}
								onChange={(value) => this.props.handleInputChange('companyName', value)}
								validate={this.state.validate}
								validationOption={{
									name: 'Company Name',
									check: true,
									customFunc: companyName => {
										if(validator.isAlphanumeric(companyName,["en-US"]))
										{
											return true;
										} 
									},
									min: 3,
								}}
								/>
							</div>
						</div>

						<div className="col-md-12">
							<div className="checkout-form-list">
								<label>Address <span className="required">*</span></label>
								<Textbox
									id='streetAddress'
									name="Street_Address"
									placeholder="Street address"
									classNameInput="form-control"
									type="text"
									maxLength={20}
									value = {this.props.form.streetAddress}
									onChange={(value) => this.props.handleInputChange('streetAddress', value)}
									validate={this.state.validate}
									onBlur={() => { }}
									validationOption={{
										name: 'Street Address',
										check: true,
										required: true,
										customFunc: streetAddress => {
											if(validator.isAlphanumeric(streetAddress,["en-US"]))
											{
												return true;
											} 
										},
										min: 3,
									}}
								/>
							</div>
						</div>

						<div className="col-md-12">
							<div className="checkout-form-list mtb-30">
								<Textbox
									id='address'
									name="Address"
									placeholder="Apartment, suite, unit etc. (optional)"
									classNameInput="form-control"
									type="text"
									value = {this.props.form.address}
									onChange={(value) => this.props.handleInputChange('address', value)}
								/>
							</div>
						</div>

						<div className="col-md-12">
							<div className="checkout-form-list mb-30">
								<label>Town / City <span className="required">*</span></label>
								<Textbox
									id='town'
									name="Town"
									placeholder="Town/ City"
									classNameInput="form-control"
									type="text"
									maxLength={20}
									value = {this.props.form.town}
									onChange={(value) => this.props.handleInputChange('town', value)}
									validate={this.state.validate}
									onBlur={() => { }}
									validationOption={{
									name: 'Town',
									check: true,
									required: true,
									customFunc: town => {
										if(validator.isAlpha(town,["en-US"]))
										{
											return true;
										} 
										else
										{
											return "Town/ City field contains only alphabets";
										}
									},
									min: 3,
									}}
								/>
							</div>
						</div>

						<div className="col-md-6">
							<div className="checkout-form-list mb-30">
								<label>State/ County <span className="required">*</span></label>
								<Textbox
									id='county'
									name="County"
									placeholder="State/ County"
									classNameInput="form-control"
									type="text"
									maxLength={20}
									value = {this.props.form.county}
									onChange={(value) => this.props.handleInputChange('county', value)}
									validate={this.state.validate}
									onBlur={() => { }}
									validationOption={{
										name: 'County',
										check: true,
										required: true,
										customFunc: county => {
											if(validator.isAlpha(county,["en-US"]))
											{
												return true;
											} 
											else
											{
												return "County field contains only alphabets";
											}
										},
										min: 3,
									}}
								/>
							</div>
						</div>

						<div className="col-md-6">
							<div className="checkout-form-list mb-30">
								<label>Postcode / Zip <span className="required">*</span></label>
								<Textbox
									id='postcode'
									name="Postcode"
									placeholder="Postcode / Zip"
									classNameInput="form-control"
									type="text"
									maxLength={20}
									value = {this.props.form.postcode}
									onChange={(value) => this.props.handleInputChange('postcode', value)}
									validate={this.state.validate}
									onBlur={() => { }}
									validationOption={{
										name: 'State',
										check: true,
										required: true,
										customFunc: postcode => {
											if(validator.isPostalCode(postcode,["IN"]))
											{
												return true;
											} 
											else
											{
												return "Invalid Postcode";
											}
										},
										min: 3,
									}}
								/>
							</div>
						</div>

						<div className="col-md-6">
							<div className="checkout-form-list mb-30">
								<label>Email Address <span className="required">*</span></label>
								<Textbox
									classNameInput="form-control"
									id='email'
									name="Email"
									type="text"
									placeholder="Enter the E-mail Address"
									maxLength={30}
									value = {this.props.form.email}
									onChange={(value) => this.props.handleInputChange('email', value)}
									validate={this.state.validate}
									onBlur={() => { }}
									validationOption={{
										name: 'Email',
										check: true,
										required: true,
										customFunc: email => {
											if(validator.isEmail(email, [{ allow_display_name: false, require_display_name: false, allow_utf8_local_part: true, require_tld: true, allow_ip_domain: false, domain_specific_validation: false }])){
												return true
											}
											else {
												return "Enter the valid Email Address"
											}
										}
									}}
								/>
							</div>
						</div>

						<div className="col-md-6">
							<div className="checkout-form-list mb-30">
								<label>Phone  <span className="required">*</span></label>
								<Textbox
									className="form-control"
									name="mobile"
									type="text"
									maxLength={10}
									validate={this.state.validate}
									placeholder="Mobile Number"
									value={(this.context.user===null)?this.props.form.mobile:this.context.user.phone}
									onChange={(value) => this.props.handleInputChange('mobile', value)}
									onBlur={() => { }}
									validationOption={{
										name: 'Mobile',
										check: true,
										required: true,
										type: "number",
										length: 10,
										min: 10,
										customFunc: phone => {
											if (validator.isMobilePhone(phone, 'en-IN')) {
												return true;
											}
											else {
												return 'Please enter a valid phone number';
											}
										}
									}}
								/>
							</div>
						</div>

						<div className="col-md-12">
							<div className="different-address">
								<div className="order-notes">
									<div className="checkout-form-list">
										<label>Order Notes</label>
										<Textarea
											id='checkoutMessage'
											name="Checkout_Mess"
											placeholder="Notes about your order, e.g. special notes for delivery."
											classNameInput="form-control"
											type="text"
											value = {this.props.form.checkoutMessage}
											onChange={(value) => this.props.handleInputChange('checkoutMessage', value)}
										/>
									</div>
								</div>
							</div>	
						</div>
					</div>
				</div>
			</div>
		)
	}
}
BillingDetailsForm.contextType = CustomerContext;

export default BillingDetailsForm 