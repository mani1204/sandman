import React from 'react'
import Header from './Header'
import BillingDetailsForm from './BillingDetailsForm'
import OrderDetails from './OrderDetails'
import Coupon from './Coupon'
import CustomerContext from "utils/context/CustomerContext"
import CartService from 'services/commerce/Cart'
import swal from 'sweetalert';

class Index extends React.Component {
	constructor(props) {
        super(props)
        this.state = {
            form: {
                country:9,
                firstName:"",
                lastName:"",
                companyName:"",
                streetAddress:"",
                town:"",
                county:"",
                postcode:"",
                email:"",
                mobile:"",
                checkoutMessage:"",
            },
            products: [],   
            totalPrice: 0
        }
        
    }

    componentDidMount(){
        this.cartService = new CartService(this.context.user)
        this.fetchCartDetails()
    }
    calculateTotal = (products) => {
        const reducer = (accumulator, currentValue) => {
            return accumulator + currentValue.product.price * currentValue.qty
        }
        const totalPrice = products.reduce(reducer, 0)
        return totalPrice
    }
    
    handleInputChange = (name,value) =>
    { 
     	this.setState({form: { ...this.state.form, ...{ [name] : value } } }); 
 	}
    
    fetchCartDetails() {
        this.cartService.list()
            .then((res) => {
                const total = this.calculateTotal(res.data.cart)
                this.setState({ products: res.data.cart,totalPrice: total})
            })
            .catch((e) => {
                swal({
                    title: "Error!",
                    text: "Please Try Again!",
                    icon: "error",
                    button: "Ok!",
                });
            })
    }

	render(){
		return (
    			<React.Fragment>
    				{ !this.context.user && <Header /> }
    				<div className="checkout-area pt-30  pb-60">
    					<div className="container">
    						<div className="row"> 
                                <BillingDetailsForm handleInputChange={(name, value) => this.handleInputChange(name, value)} form = {this.state.form}/>
                                <OrderDetails products={this.state.products} totalPrice={this.state.totalPrice} />
                            </div>
    					</div>
    				</div>	
    			</React.Fragment>
		);
	}
}
Index.contextType = CustomerContext;
export default Index