import React from 'react';

class Coupon extends React.Component {
  constructor(props) {
    super(props);
    this.state = {isClicked: false};
  }

  handleClickToggle = () => {
    const isClicked = this.state.isClicked;
    this.setState({isClicked: !isClicked});
  }

  render() {
    const isClicked = this.state.isClicked;
    return (
      <div>
        <div className="coupon-area">
        <div className="container">
          <div className="coupon-accordion">
            <h3>Have a coupon? <span id="showcoupon" onClick={this.handleClickToggle}>Click here to enter your code</span></h3>
          </div>
        </div>
      </div>
        <div className="container">
          <div className="coupon-info">
            {isClicked && 
              <form action="#">
                <p className="checkout-coupon">
                  <input type="text" className="code" placeholder="Coupon code" />
                  <input type="submit" value="Apply Coupon" />
                </p>
              </form> 
            }
          </div>
        </div>
      </div>
    );
  }
}
export default Coupon
