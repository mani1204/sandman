import React, { Component } from 'react'
import image1 from 'packs/images/1.jpg'
import image2 from 'packs/images/2.jpg'
import image4 from 'packs/images/4.jpg'
class Index extends Component {
    constructor() {
        super()
        this.state = {
            products: [
                {
                    id: 0,
                    image: image1,
                    prodname: "ProductName",
                    description: "Impressively thin and light,  HP EliteBook 840 empowers users to create, connect, and collaborate, using enterprise-className performance",
                    price: 4000,
                    color: "balck",
                    stock: "Stock In",
                    deleteIcon: "",
                    addtocart: "",
                    rating: ""
                },
                {
                    id: 1,
                    image: image2,
                    prodname: "ProductName",
                    description: "Impressively thin and light,  HP EliteBook 840 empowers users to create, connect, and collaborate, using enterprise-className performance",
                    price: 2500,
                    color: "balck",
                    stock: "Stock Out",
                    deleteIcon: "",
                    addtocart: "",
                    rating: ""
                },
                {
                    id: 2,
                    image: image4,
                    prodname: "ProductName",
                    description: "Impressively thin and light,  HP EliteBook 840 empowers users to create, connect, and collaborate, using enterprise-className performance",
                    price: 3500,
                    color: "balck",
                    stock: "Stock In",
                    deleteIcon: "",
                    addtocart: "",
                    rating: ""
                }
            ]
        }
    }
    clickDeleteIcon = (productId) => {
        const afterDeleteProducts = this.state.products.filter((item) => item.id !== productId);
        this.setState({ products: afterDeleteProducts })
    }
    _renderCartProducts = (productProps) => {
        let data = [];
        for (let i = 0; i < this.state.products.length; i++) {
            if (productProps === "image") {
                data.push(<td key={this.state.products[i]["id"]}><img src={this.state.products[i][productProps]}/></td>)
            }
            else if (productProps === "deleteIcon") {
                data.push(<td key={this.state.products[i]["id"]} onClick={() => this.clickDeleteIcon(this.state.products[i]["id"])} className="product-description"><button className="btn-danger">Delete</button></td>)
            }
            else if (productProps === "addtocart") {
                data.push(<td key={this.state.products[i]["id"]} className="product-description">
                    <a className="compare-cart text-uppercase" href="#">add to cart</a>
                </td>)
            }
            else if (productProps === "price") {
                data.push(<td key={this.state.products[i]["id"]}><i className="fas fa-rupee-sign"></i>{this.state.products[i][productProps]}</td>)
            }
            else if (productProps === "rating") {
                data.push(<td key={this.state.products[i]["id"]} className="product-description">
                    <div className="product-rating">
                        <i className="fa fa-star"></i>
                        <i className="fa fa-star"></i>
                        <i className="fa fa-star"></i>
                        <i className="fa fa-star"></i>
                        <i className="fa fa-star"></i>
                    </div>
                </td>)
            }
            else {
                data.push(<td key={this.state.products[i]["id"]}>{this.state.products[i][productProps]}</td>)
            }
        }
        return data
    }
    render() {
        const fields = [{ title: "Product", value: "image" }, { title: "ProductName", value: "prodname" },
        { title: "Description", value: "description" }, { title: "Price", value: "price" },
        { title: "Color", value: "color" }, { title: "Stock", value: "stock" },
        { title: "Add to Cart", value: "addtocart" }, { title: "Delete", value: "deleteIcon" },
        { title: "Rating", value: "rating" }
        ]
        return (
            <div className="compare-product pb-40">
                <div className="container">
                    <div className="table-responsive">
                        <table className="table text-center compare-content">
                            <tbody>
                                {this.state.products.length === 0 ?
                                    <tr><td className="empty-cart" colSpan="6">No items to Compare</td></tr> :
                                    <React.Fragment>
                                        {fields.map((item, index) => (
                                            <tr key={index}>
                                                <td className="compare-table-attr-width">{item.title}</td>
                                                {this._renderCartProducts(item.value)}
                                            </tr>
                                        ))}
                                    </React.Fragment>
                                }
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        )
    }
}
export default Index