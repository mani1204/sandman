import React, { Component } from 'react'
function FinalPricing(props) {
    return (
        <div className="cart_totals">
            <h2>Cart Totals</h2>
            <br />
            <table>
                <tbody className="cart_totals_price">
                    <tr className="cart-subtotal">
                        <th>Subtotal</th>
                        <td>
                            <span className="amount">
                                <i className="fas fa-rupee-sign cart-subtotal-rupee-symbol mr-1"></i>
                                {props.subTotal}
                            </span>
                        </td>
                    </tr>
                    <tr className="order-total">
                        <th>Total</th>
                        <td>
                            <strong>
                                <span className="amount">
                                    <i className="fas fa-rupee-sign cart-total-rupee-symbol mr-1"></i>
                                    {props.totalPrice}
                                </span>
                            </strong>
                        </td>
                    </tr>
                </tbody>
            </table >
            <div className="wc-proceed-to-checkout">
                <a href="../commerce/checkout">Proceed to Checkout</a>
            </div>
        </div>
    )
}
function ContinueBtn() {
    return (
        <div className="buttons-cart">
            <a href="/">Continue Shopping</a>
        </div>
    )
}
function ContinueShopping(props) {
    return (
        <div className="container">
            <div className="row">
                <div className="col-lg-8 col-md-7 col-12 cart-left-right-padding">
                    <ContinueBtn />
                </div>
                <div className="col-lg-4 col-md-12 col-12 cart-left-right-padding">
                    <FinalPricing totalPrice={props.totalPrice} subTotal={props.subTotal} />
                </div>
            </div>
        </div>
    )
}
export default ContinueShopping