import React from 'react'
import CartTable from './CartTable'
import ContinueShopping from './ContinueShopping'
import CartService from 'services/commerce/Cart'
import image1 from 'packs/images/1.jpg'
import swal from 'sweetalert'
import find from 'lodash/find'
import CustomerContext from "utils/context/CustomerContext"

class Index extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			products: [],
			totalPrice: 0,
			subTotal: 0,
		}
	}
	// cartDecrement = (productId) => {
	// 	let products = this.state.products.slice()
	// 	let product = find(products, (product) => {
	// 		return product.id === productId
	// 	})
	// 	if (product.qty == 0) {
	// 		const reducer = (accumulator, currentValue) => {
	// 			return accumulator + currentValue.product.price * currentValue.qty
	// 		}
	// 		let totalPrice = products.reduce(reducer, 0)
	// 		this.setState({ products, totalPrice, subTotal: totalPrice })
	// 	}
	// 	else {
	// 		product.qty = product.qty - 1
	// 		const reducer = (accumulator, currentValue) => {
	// 			return accumulator + currentValue.product.price * currentValue.qty
	// 		}
	// 		let totalPrice = products.reduce(reducer, 0)
	// 		this.setState({ products, totalPrice, subTotal: totalPrice })
	// 	}
	// }
	// cartIncrement = (productId) => {
	// 	console.log(productId)
	// 	let products = this.state.products.slice()
	// 	let product = find(products, (product) => {
	// 		return product.id === productId
	// 	})
	// 	if (product.qty == 10) {
	// 		const reducer = (accumulator, currentValue) => {
	// 			return accumulator + currentValue.product.price * currentValue.qty
	// 		}
	// 		let totalPrice = products.reduce(reducer, 0)
	// 		this.setState({ products, totalPrice, subTotal: totalPrice })
	// 	}
	// 	else {
	// 		product.qty = product.qty + 1
	// 		const reducer = (accumulator, currentValue) => {
	// 			return accumulator + currentValue.product.price * currentValue.qty
	// 		}
	// 		let totalPrice = products.reduce(reducer, 0)
	// 		this.setState({ products, totalPrice, subTotal: totalPrice })
	// 	}
	// }
	clickDeleteIcon = (productId) => {
		this.cartService.delete(productId)
			.then((res) => {
				if (res.data.success) {
					const wholeProducts = this.state.products.slice()
					const filteredProducts = wholeProducts.filter((item) => item.id !== productId);
					const totalPrice = this.calculateTotal(filteredProducts)
					this.setState({ products: filteredProducts, totalPrice, subTotal: totalPrice })
					this.props.onCartDelete()
				}
				else {
					swal({
						title: "Error!",
						text: "Please Try Again!",
						icon: "error",
						button: "Ok!",
					});
				}
			})
			.catch((e) => {
				swal({
					title: "Error!",
					text: "Please Try Again!",
					icon: "error",
					button: "Ok!",
				});
			})
	}
	handleQuantityChange = (productId, count) => {
		const products = this.state.products.slice()
		let product = find(products, (product) => {
			return product.id === productId
		})
		product.qty = count
		const totalPrice = this.calculateTotal(products)
		this.setState({ products, totalPrice, subTotal: totalPrice })
		if (count > 0) {
			this.cartService.update(product.id, {product: {qty:count}}, {
				headers: {
					'Content-Type': 'application/json',
					'X-CSRF-Token': document.getElementsByName('csrf-token')[0].content
				}
			})
				.then(res => {
					if (res.data.success) {
						this.props.onCartInputChange()
					}
					else {
						swal({
							title: "Error!",
							text: "Please Try Again!",
							icon: "error",
							button: "Ok!",
						});
					}
				})
				.catch((e) => {
					swal({
						title: "Error!",
						text: "Please Try Again!",
						icon: "error",
						button: "Ok!",
					});
				})
		}
	}
	componentDidMount() {
		this.cartService = new CartService(this.context.user)
		this.fetchCartDetails()
	}
	calculateTotal = (products) => {
		const reducer = (accumulator, currentValue) => {
			return accumulator + currentValue.product.price * currentValue.qty
		}
		let totalPrice = products.reduce(reducer, 0)
		return totalPrice
	}
	fetchCartDetails() {
		this.cartService.list()
			.then((res) => {
				let totalPrice = this.calculateTotal(res.data.cart)
				this.setState({ products: res.data.cart, totalPrice, subTotal: totalPrice })
			})
			.catch((e) => {
				swal({
					title: "Error!",
					text: "Please Try Again!",
					icon: "error",
					button: "Ok!",
				});
			})
	}
	render() {
		return (
			<div>
				<CartTable cartDecrement={(productId) => { this.cartDecrement(productId) }}
					cartIncrement={(productId) => { this.cartIncrement(productId) }}
					clickDeleteIcon={(productId) => this.clickDeleteIcon(productId)}
					products={this.state.products} image={image1}
					handleQuantityChange={(productId, count) => this.handleQuantityChange(productId, count)}
				/>
				<ContinueShopping totalPrice={this.state.totalPrice} subTotal={this.state.subTotal}/>
			</div>
		);
	}
}

Index.contextType = CustomerContext;

export default Index