import React, { Component } from 'react'
import CounterInput from "react-counter-input";

class CartTable extends Component {
    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="table-content col-md-12 table-responsive mb-50">
                        <table>
                            <thead>
                                <tr>
                                    <th className="product-thumbnail">Image</th>
                                    <th className="product-name">Product</th>
                                    <th className="product-price">Price</th>
                                    <th className="product-quantity">Quantity</th>
                                    <th className="product-subtotal">Total</th>
                                    <th className="product-remove">Remove</th>
                                </tr>
                            </thead>
                            <tbody>
                                {this.props.products.length <= 0 ? <tr><td className="empty-cart" colSpan="6">No items in the cart</td></tr> : this.props.products.map((product, index) => (
                                    <tr key={index}>
                                        <td className="product-thumbnail">
                                            <a href="#"><img src={this.props.image} alt="cart-image" /></a>
                                        </td>
                                        <td className="product-name">
                                            <a href="#">{product.product.name}</a>
                                        </td>
                                        <td className="product-price">
                                            <div className="amount">
                                                <i className="fas fa-rupee-sign cart-rupee-symbol mr-1"></i>
                                                {product.product.price}
                                            </div>
                                        </td>
                                        <td className="product-quantity">
                                            <div className="cart-increment-decrement">
                                                {/* <div onClick={() => this.props.cartDecrement(product.id)}><i className="fa fa-minus increment-decremnet-cart mr-10" aria-hidden="true"></i></div> */}
                                                <input className="number text-center" type="number" min="1" onChange={(e) => this.props.handleQuantityChange(product.id, e.target.value)} value={product.qty} />
                                                {/* <div onClick={() => this.props.cartIncrement(product.id)}><i className="fa fa-plus increment-decremnet-cart ml-10" aria-hidden="true"></i></div> */}
                                            </div>
                                        </td>
                                        <td className="product-subtotal">
                                            <i className="fas fa-rupee-sign cart-rupee-symbol mr-1"></i>
                                            {product.qty * product.product.price}
                                        </td>
                                        <td className="product-remove">
                                            <div className="cart-delete-icon d-inline-block" onClick={(e) => this.props.clickDeleteIcon(product.id, e.target.value)}>
                                                <i className="fa fa-times" aria-hidden="true"></i>
                                            </div>
                                        </td>
                                    </tr>
                                ))}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        );
    }
}

export default CartTable;
