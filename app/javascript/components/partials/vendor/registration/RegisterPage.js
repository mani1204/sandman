import React from 'react';
import ReactDOM from 'react-dom';
import RegisterForm from 'components/partials/vendor/registration/Form';
class RegisterPage extends React.Component {
  constructor(props){
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  handleSubmit(e){
    console.log(e);
  }
  render(){
    return (
      <div className="register-account pb-60">
        <div className="container">
          <div className="row">
            <div className="col-sm-12">
              <div className="register-title">
                <h3 className="mb-10">REGISTER ACCOUNT</h3>
                <p className="mb-10">If you already have an account with us, please 
                  <a href="login"><b> login</b></a> at the login page.
                </p>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-sm-12">
              <RegisterForm onSubmit={this.handleSubmit}/>
            </div>
          </div>
        </div>
      </div>
    );
  }
}


export default RegisterPage