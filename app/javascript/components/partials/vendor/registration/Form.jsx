import React , { Component } from 'react';
import ReactDOM from 'react-dom';
import { Textbox, Select, Checkbox, Radiobox } from 'react-inputs-validation'
import validator from 'validator';
import 'react-inputs-validation/lib/react-inputs-validation.css';
import reduce from "lodash/reduce"

class Form extends React.Component {
    constructor(props)
    {
        super(props)
        this.state= 
        { 
            subscription:false,
            isAgreementChecked :false,
            isChecked:false,
            validate:false,
            form: {
                vendorName : "",
                companyName: "",
                gstNumber: "",
                vatNumber: "",
                email: "",
                mobile: "",
                city: "",
                password: "",
                confirmPassword: "",
                subscribeValue:"",
                checkboxValue:"",
                isAgreementChecked: 0
            },
            error: {
                vendorNameError:true,
                companyNameError:true,
                gstNumberError:true,
                vatNumberError:true,
                emailError:true,
                mobileError:true,
                passwordError:true,
                confirmPasswordError:true,
                subscribeValueError:true,
            }
        }
        this.handleInputChange = this.handleInputChange.bind(this);
    }

    handleInputChange(value,name,isCheckbox = false)
    {
        this.setState({form: {...this.state.form, ...{[name]: value}}});
    }
    toggleValidating(validate) 
    {
        this.setState({ validate })
    }

    validateForm = (e) => 
    {
        e.preventDefault()
        this.toggleValidating(true);
        if(reduce([this.state.error],function(a,b){   
                    return !a && !b
                })
        )
        {
            const form = () => 
            {
              this.state.form             
            }
        }
        this.props.onSubmit();
    }

    render()
    {
        const {validate,isAgreementChecked,isChecked} =this.state
        return (
            <div>
                <form className="form-horizontal" method = 'post' onSubmit={this.validateForm}>
                    <legend>Your Personal Details</legend>
                    <div className="form-group">
                        <label className="control-label" htmlFor="Vendor_name">
                            <span className="require">*</span>Vendor Name
                        </label>
                        <div className="col-sm-10">
                                <Textbox
                                    id='vendorName'
                                    classNameInput="form-control"
                                    name="Vendor_Name"
                                    type="text"
                                    placeholder="Enter the Vendor Name"
                                    maxLength={20}
                                    value = {this.state.form.vendorName}
                                    onChange={(vendorName) => {this.handleInputChange(vendorName,"vendorName")}}
                                    validate={validate}
                                    onBlur={() => { }}
                                    validationOption={{
                                        name: 'Vendor Name',
                                        check:true,
                                        required: true,
                                        customFunc: vendorName => validator.isAlpha(vendorName,["en-US"])? true : "Vendor Name field contains only alphabets",
                                        min: 3,
                                    }}
                                />
                        </div>
                    </div>
                    <div className="form-group">
                        <label className="control-label" htmlFor="Company_Name">
                            <span className="require">*</span>Company Name
                        </label>
                        <div className="col-sm-10">
                            <Textbox
                                id='companyName'
                                name="Company_Name"
                                classNameInput="form-control"
                                type="text"
                                placeholder="Enter the Company Name"
                                maxLength={20}
                                value = {this.state.form.companyName}
                                onChange={(companyName) => {this.handleInputChange(companyName,"companyName")}}
                                validate={validate}
                                onBlur={() => { }}
                                 validationOption={{
                                        name: 'Company Name',
                                        check: true,
                                        required: true,
                                        customFunc: companyName => {
                                            if(validator.isAlphanumeric(companyName,["en-US"]))
                                            {
                                                return true;
                                            } 
                                            else
                                            {
                                                return "Company Name field contains only alphabets";
                                            }
                                        },
                                        min: 3,
                                    }}
                            />
                        </div>
                    </div>
                    <div className="row">
                        <div className="form-group col-sm-5">
                            <label className="control-label" htmlFor="gst_Number">
                                <span className="require">*</span>GST Number
                            </label>
                            <div className="col">
                                <Textbox
                                    id='gstNumber'
                                    classNameInput="form-control"
                                    name="Gst_Number"
                                    type="text"
                                    placeholder="Enter the GST Number"
                                    maxLength={15}
                                    value = {this.state.form.gstNumber}
                                    onChange={(gstNumber) => {this.handleInputChange(gstNumber,"gstNumber")}}
                                    validate={validate}    
                                    onBlur={() => { }}
                                    validationOption={{
                                        name: 'GST Number',
                                        check: true,
                                        required: true,
                                        customFunc: gstNumber => {
                                           const reg = /^[0-9]{2}[A-Z]{5}[0-9]{4}[A-Z]{1}[1-9A-Z]{1}Z[0-9A-Z]{1}$/
                                           if (reg.test(String(gstNumber).toUpperCase())) 
                                            {
                                                return true;
                                            }
                                            else 
                                            {
                                                return 'Please enter a valid GST';
                                            }
                                        },
                                        min: 3,
                                    }}
                                />
                            </div>
                        </div>
                        <div className="form-group col-sm-5">
                            <label className="control-label" htmlFor="vat_Number">
                                <span className="require"></span>VAT Number
                                </label>
                                <div className="col">
                                <Textbox
                                    classNameInput="form-control"
                                    id='vatNumber'
                                    name="Vat_Number"
                                    type="text"
                                    placeholder="Enter the VAT number"
                                    maxLength={12}
                                    value = {this.state.form.vatNumber}
                                    onChange={(vatNumber) => {this.handleInputChange(vatNumber,"vatNumber")}}
                                    validate={validate}    
                                    onBlur={() => { }}
                                    validationOption={{
                                        name: 'VAT Number',
                                        check: true,
                                        required: true,
                                        customFunc: vatNumber => {
                                            if(validator.isAlphanumeric(vatNumber,["en-US"]))
                                            {
                                                return true;
                                            } 
                                        },
                                        min: 3,
                                    }}
                                />
                            </div>
                        </div>
                    </div>
                    <div className="form-group">
                        <label className="control-label" htmlFor="email">
                            <span className="require">*</span>E-mail Address
                        </label>
                        <div className="col-sm-10">
                            <Textbox
                                classNameInput="form-control"
                                id='email'
                                name="Email"
                                type="text"
                                placeholder="Enter the E-mail Address"
                                maxLength={30}
                                value = {this.state.form.email}
                                onChange={(email) => {this.handleInputChange(email,"email")}}
                                validate={validate}
                                onBlur={() => { }}
                                validationOption={{
                                    name: 'Email',
                                    check: true,
                                    required: true,
                                    customFunc: email => {
                                        if(validator.isEmail(email, [{ allow_display_name: false, require_display_name: false, allow_utf8_local_part: true, require_tld: true, allow_ip_domain: false, domain_specific_validation: false }])){
                                            return true
                                        }
                                        else {
                                            return "Enter the valid Email Address"
                                        }
                                    }
                                }}
                            />
                        </div>
                    </div>
                    <div className="form-group">
                        <label className="control-label" htmlFor="mobile">
                            <span className="require">*</span>Mobile Number
                        </label>
                        <div className="col-sm-10">
                            <Textbox
                                classNameInput="form-control"
                                id='mobile'
                                name="Mobile"
                                type="text"
                                placeholder="Enter the Mobile Number"
                                maxLength={10}
                                value = {this.state.form.mobile}
                                onChange={(mobile) => {this.handleInputChange(mobile,"mobile")}}
                                validate={validate}
                                onBlur={() => { }}
                                validationOption={{
                                    name: 'Mobile',
                                    check: true,
                                    required: true,
                                    type: "number",
                                    lenght: 10,
                                    min: 10,
                                    customFunc: phone => {
                                        if (validator.isMobilePhone(phone, 'en-IN')) {
                                            return true;
                                        }
                                        else {
                                            return 'Please enter a valid phone number';
                                        }
                                    }
                                }}
                            />
                        </div>
                    </div>
                    <div className="form-group">
                        <label className="control-label" htmlFor="city">
                            <span className="require">*</span>City
                        </label>
                        <div className="col-sm-10">
                            <Select 
                                id = "city"
                                name='City' 
                                onBlur={() => {}}
                                value={this.state.form.city} 
                                optionList={[{id:'',name:'Please select your city'} , {id:'1',name:'Chennai'} , {id:'2',name:'Bangalore'} , {id:'3',name:'Delhi'} , {id:'4',name:'Mumbai'}]}
                                customStyleContainer={{padding : '1%' , border: '1px solid #e5e5e5'}}
                                onChange={(city) => {
                                       this.handleInputChange(city,'city')
                                    }
                                }
                                validate={validate}
                                validationOption={{
                                    name: 'City',
                                    check: true,
                                    required: true,
                                }}

                            />
                        </div>
                    </div>
                    <legend>Your Password</legend>
                    <div className="form-group">
                        <label className="control-label" htmlFor="password">
                            <span className="require">*</span>Password
                        </label>
                        <div className="col-sm-10">
                            <Textbox
                                classNameInput="form-control"
                                id='password'
                                name="password"
                                type="password"
                                placeholder="Enter the Password"
                                maxLength={20}
                                value = {this.state.form.password}
                                onChange={(password) => {this.handleInputChange(password,"password")}}
                                validate={validate}
                                onBlur={() => { }}
                                validationOption={{
                                    name: 'Password',
                                    check: true,
                                    required: true,
                                    customFunc: password => {
                                        if(validator.isAlphanumeric(password,["en-US"]))
                                        {
                                            return true;
                                        } 
                                    },
                                }}
                            />
                        </div>
                    </div>
                    <div className="form-group pb-60">
                        <label className="control-label" htmlFor="confirm_Password">
                            <span className="require">*</span>Confirm Password
                        </label>
                        <div className="col-sm-10">
                            <Textbox
                                classNameInput="form-control"
                                id='confirmPassword'
                                name="Confirm_Password"
                                type="password"
                                placeholder="Confirm Password"
                                maxLength={20}
                                value = {this.state.form.confirmPassword}
                                onChange={(confirmPassword) => {this.handleInputChange(confirmPassword,"confirmPassword")}}
                                validate={validate}
                                onBlur={() => { }}
                                validationOption={{
                                    name: 'Confirm Password',
                                    check: true,
                                    required: true,
                                    customFunc: confirmPassword => {
                                        if(validator.isAlphanumeric(confirmPassword,["en-US"]))
                                            {
                                                return true;
                                            } 
                                        },
                                    compare:this.state.form.password,
                                }}
                            />
                        </div>
                    </div>
                    {/*<legend>Newsletter</legend>
                    <div className="form-group">
                        <label className="control-label">Subscribe</label>
                        <div className="col-sm-10">
                            <label className = "radio-inline">
                                <Radiobox 
                                id = "radiobox"
                                name="subscription"
                                value = {this.state.form.subscription}
                                onChange={(subscribeValue) => {this.handleInputChange(subscribeValue,"subscription");}}                         
                                optionList={[{id:'1', classNameInput:'radio-inline' , name:'Yes'} , {id:'2' , classNameInput:'radio-inline' , name:'No'}]} 
                                validate={validate}
                                validationOption={{
                                    name: 'option',
                                    check: true,
                                    required: true,
                                }}
                                customStyleContainer={{
                                    display: 'flex',
                                    justifyContent: 'flex-start'
                                }} 
                                customStyleOptionListItem={{ marginRight: '20px' }}
                                />
                            </label>    
                        </div>
                    </div> */}
                    <div className="buttons newsletter-input ">
                        <div className="register-newsletter-input">
                            <div className='agree'>
                                <Checkbox 
                                    id="checkbox" 
                                    name = "isAgreementChecked"
                                    type="checkbox" 
                                    value = {this.state.form.checkboxValue}
                                    onChange={(checkboxValue) => {
                                    this.handleInputChange(checkboxValue,'isAgreementChecked',isChecked:true);}}
                                    checked = {this.state.form.isAgreementChecked}
                                    validate={validate}
                                    validationOption={{
                                        name: 'Privacy Policy',
                                        check: true,
                                        required: true,
                                    }}
                                    labelHtml={
                                        <div style={{ color: '#4a4a4a', marginTop: '2px' }}>
                                        <span>I have read and agree to the 
                                            <a href="#" className="agree"><b> Privacy Policy </b></a>
                                        </span>
                                        </div>
                                    }
                                />
                            </div>
                            <button type="submit" className="newsletter-btn reg-button"> Continue</button>
                        </div>
                    </div>
                </form>
            </div>
        );
    }
}

export default Form
            