import React, { Component } from 'react';
import MobileForm from './MobileForm';
import OtpForm from './OtpForm'
import PasswordForm from './PasswordForm'
import axios from 'axios'

class Index extends Component {
    constructor(props) {
        super(props)
        this.state = {
            form: {
                otp: "",
                newPassword: "",
                confirmPassword: "",
                mobile: "",
            },
            currentPage: 1,
        }
    }
    backToMainPage = (currentPage,e) => {
        e.preventDefault();
        this.setState({ currentPage, form: { ...this.state.form, ...{ otp: "", mobile: "" } } })
    }
    handleSubmitMobileForm = () => {
        axios.post(`/vendors/reset_password`, { mobileNumber: this.state.form.mobile }, {
            headers: {
                'Content-Type': 'application/json',
                'X-CSRF-Token': document.getElementsByName('csrf-token')[0].content
            }
        })
            .then(res => {
                if (res.data.success) {
                }
                else {
                }
            })
            .catch(e => {
            })
    }
    handleSubmitOTPForm=()=>{
        axios.post(`/vendors/reset_password`, { otp: this.state.form.otp }, {
            headers: {
                'Content-Type': 'application/json',
                'X-CSRF-Token': document.getElementsByName('csrf-token')[0].content
            }
        })
            .then(res => {
                if (res.data.success) {
                 
                }
                else {
                }
            })
            .catch(e => {
            })
    }
    submitPasswordForm=()=>{
        axios.post(`/vendors/reset_password`, { password: this.state.form.newPassword }, {
            headers: {
                'Content-Type': 'application/json',
                'X-CSRF-Token': document.getElementsByName('csrf-token')[0].content
            }
        })
            .then(res => {
                if (res.data.success) {
                 
                }
                else {
                }
            })
            .catch(e => {
            })
    }
    handleNewPassChange = (newPassword) => {
        this.setState({ form: { ...this.state.form, ...{ newPassword } } })
    }
    handleChangeOTPForm = (otp) => {
        this.setState({ form: { ...this.state.form, ...{ otp: otp } } })
    }
    updateCurrentPage = (page) => {
        this.setState({ currentPage: page })
    }
    handleMobileNumberChange = (mobile, e) => {
        this.setState({ form: { ...this.state.form, ...{ mobile: mobile } } })
    }
    handleConfirmPassChange = (confirmPassword) => {
        this.setState({ form: { ...this.state.form, ...{ confirmPassword } } })
    }
    renderPageSection = () => {
        if (this.state.currentPage === 1) {
            return <MobileForm handleMobileNumberChange={(mobile) => { this.handleMobileNumberChange(mobile) }}
                mobile={this.state.form.mobile} updateCurrentPage={(page) => this.updateCurrentPage(page)}
                handleSubmitMobileForm={this.handleSubmitMobileForm}
            />
        }
        if (this.state.currentPage === 2) {
            return <OtpForm backToMainPage={this.backToMainPage.bind(this,1)}
            handleChangeOTPForm={(otp) => this.handleChangeOTPForm(otp)} updateCurrentPage={(page) => this.updateCurrentPage(page)}
                otp={this.state.form.otp} handleSubmitOTPForm={this.handleSubmitOTPForm}
            />
        }
        if (this.state.currentPage === 3) {
            return <PasswordForm handleConfirmPassChange={(confirmPassword, e) => { this.handleConfirmPassChange(confirmPassword) }}
                confirmPassword={this.state.form.confirmPassword} handleNewPassChange={(newPassword, e) => { this.handleNewPassChange(newPassword) }}
                newPassword={this.state.form.newPassword} backToMainPage={this.backToMainPage.bind(this,1)} submitPasswordForm={this.submitPasswordForm}
            />
        }
    }
    render() {
        return (
            <div className="container">
                <div className="register-title">
                    <h3 className="mb-10">RECOVER  PASSWORD</h3>
                </div>
                {this.renderPageSection()}
            </div>
        );
    }
}
export default Index;

