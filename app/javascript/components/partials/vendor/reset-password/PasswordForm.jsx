import React, { Component } from 'react'
import { Textbox } from 'react-inputs-validation'
import validator from 'validator';
class PasswordForm extends Component {
    constructor(props) {
        super(props)
        this.state = {
            passwordError: true,
            validate: false,
        }
    }
    handleSubmitPassword = (e) => {
        e.preventDefault()
        this.setState({ validate: true })
        if (!this.state.passwordError) {
            this.props.submitPasswordForm()
        }
    }
    render() {
        return (
            <form className="password-form-width">
                <div id="passwordFormControl">
                    <label>Password</label><br />
                    <Textbox
                        id="newPassword"
                        tabIndex="1"
                        name="newPassword"
                        type="password"
                        validate={this.state.validate}
                        validationCallback={res => this.setState({ passwordError: res, validate: true })}
                        maxLength={15}
                        value={this.props.newPassword}
                        onChange={this.props.handleNewPassChange}
                        onBlur={() => { }}
                        validationOption={{
                            name: 'Password',
                            check: true,
                            required: true,
                            type: "string",
                            min: 8,
                            max: 15,
                            customFunc: newPassword => {
                                const reg = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/
                                if (reg.test(String(newPassword))) {
                                    return true;
                                } else {
                                    return 'Password must contain atleast one letter and number';
                                }
                            }
                        }
                        }
                    />
                    <label className="mt-9">Confirm Password</label><br />
                    <Textbox
                        id="confirmPassword"
                        tabIndex="1"
                        name="confirmPassword"
                        type="password"
                        value={this.props.confirmPassword}
                        onChange={this.props.handleConfirmPassChange}
                        onBlur={() => { }}
                        validationOption={{
                            name: 'Confirm Password',
                            check: true,
                            required: true,
                            type: "string",
                            compare: this.props.newPassword
                        }}
                    />
                </div>
                <p>{this.props.errorPass}</p>
                <button onClick={this.props.backToMainPage} className="return-customer-btn mb-30 mr-20">Back</button>
                <button onClick={this.handleSubmitPassword} className="return-customer-btn  mr-20">Submit</button>
            </form>
        );
    }
}
export default PasswordForm;