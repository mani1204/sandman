import React, { Component } from 'react';
import { Textbox } from 'react-inputs-validation'
import validator from 'validator';
import swal from 'sweetalert';
class MobileForm extends Component {
    constructor(props) {
        super(props)
        this.state = {
            mobileError: true,
            validate: false,
        }
    }
    sendingMobileNo = (e) => {
        e.preventDefault();
        this.setState({ validate: true });
        if (this.props.mobileValue === "") {
            return
        }
        else if (!this.state.mobileError) {
            this.props.handleSubmitMobileForm()
            this.props.updateCurrentPage(2)
        }
    }
    render() {
        return (
            <div>
                <form onSubmit={this.sendingMobileNo} className="form-horizontal pb-100" >
                    <fieldset>
                        <legend>Your Recover Phone Number</legend>
                        <div className="form-groups">
                            <label className="mobile-form control-label" ><span className="require">*</span>Enter your mobile number here...</label>
                            <div className="col-10 p-0">
                                <div id="mobileFormControl">
                                    <Textbox
                                        tabIndex="1"
                                        className="form-control"
                                        name="mobile"
                                        type="text"
                                        maxLength={10}
                                        validate={this.state.validate}
                                        validationCallback={res => this.setState({ mobileError: res, validate: true })}
                                        placeholder="Enter Your Mobile Number"
                                        value={this.props.mobile}
                                        onChange={this.props.handleMobileNumberChange}
                                        onBlur={() => { }}
                                        validationOption={{
                                            name: 'Mobile',
                                            check: true,
                                            required: true,
                                            type: "number",
                                            length: 10,
                                            min: 10,
                                            customFunc: phone => {
                                                if (validator.isMobilePhone(phone, 'en-IN')) {
                                                    return true;
                                                }
                                                else {
                                                    return 'Please enter a valid phone number';
                                                }
                                            }
                                        }}
                                    />
                                </div>
                            </div>
                        </div>
                    </fieldset>
                    <div className="buttons newsletter-input">
                        <div className="mobileFormSubmitBtn">
                            <button type="submit" className="return-customer-btn mr-20">SEND</button>
                        </div>
                    </div>
                </form>
            </div>
        );
    }
}
export default MobileForm;