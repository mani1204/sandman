import React, { Component } from 'react';
import OtpInput from 'react-otp-input';
class OtpForm extends Component {
  constructor(props) {
    super(props)
    this.state = {
      error: {
        otpError: ""
      }
    }
  }
  validateOTP = () => {
    if (this.props.otp === "") {
      this.setState({ error: { ...this.state.error, ...{ otpError: "Please enter OTP " } } })
      return
    }
    else if (this.props.otp.length < 4) {
      this.setState({ error: { ...this.state.error, ...{ otpError: "Enter 4 digit OTP number " } } })
      return
    }
    else{
      this.props.handleSubmitOTPForm()
      this.props.updateCurrentPage(3)
    }
  }
  render() {
    return (
      <div id="otpForm">
        <div className="otp-sent-msg">OTP has been sent to your Mobile Number</div>
        <div className="otp-box">
          <OtpInput
            onChange={this.props.handleChangeOTPForm}
            numInputs={4}
            separator={<span id="otpBoxSeperator">-</span>}
            shouldAutoFocus={true}
          />
          <p>{this.props.otpError}</p>
        </div>
        <div className="otp-error-msg text-danger">{this.state.error.otpError}</div>
        <button onClick={this.validateOTP} className="return-customer-btn mb-30 mt-30 mr-20">Submit</button>
        <a className="otp-resend" href="">Resend</a>
      </div>
    )
  }
}
export default OtpForm