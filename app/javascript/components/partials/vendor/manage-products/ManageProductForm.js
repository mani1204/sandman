import React from 'react'
import ManageProductContext from 'utils/context/vendor/ManageProductContext'
import { Textbox, Checkbox } from 'react-inputs-validation'
import validator from 'validator';

class ManageProductForm extends React.Component {
	constructor(props) {
        super(props)
        this.state = {
        	errors:{
	        	quantityError:"",
	        	minQtyError:"",
	        	priceError:""
	        },
	        validate:false,
        } 
    }   
	render(){
		const {isChecked} =this.state
		return (
			<React.Fragment>					
				<div className="thubnail-desc fix">
	                <h3 className="product-header">{this.context.selectedProduct.name}</h3>
	                <p className="ptb-20">{this.context.selectedProduct.description}</p>
	            </div>
	            <form className="form-horizontal" method = 'post' onSubmit={this.validateForm}>
	            	<div className='agree'>
	            	    <Checkbox 
	            	        id="checkbox"
                            name="isPriceOnDemand"
	            	        type="checkbox" 
	            	        value = {this.context.productForm.checkboxValue}
	            	        onChange={(checkboxValue) => this.props.handleInputChange(checkboxValue,'isPriceOnDemand',isChecked:true)}
                            checked = {this.context.productForm.isPriceOnDemand}
	            	        validationOption={{
	            	            name: 'Privacy Policy',
	            	            check: true,
	            	            required: true,
	            	        }}
	            	        labelHtml={
		            	        <div style={{ color: '#4a4a4a', marginTop: '8px' }}>
		            	    	    <span>Price on demand</span>
		            	        </div>
		            	    }
	            		/>
					</div>
                    <div className="form-group">
                        <label className="control-label" htmlFor="quantity">
                            <span className="require">*</span>Quantity
                        </label>
                        <div className="col-sm-10">
                                <Textbox
                                    id='quantity'
                                    classNameInput="form-control"
                                    name="Quantity"
                                    type="number"
                                    placeholder="Quantity"
                                    value = {this.context.productForm.quantity}
                                    onChange={(value) => this.props.handleInputChange(value,"quantity") }
                                    onBlur={() => { }}
                                    validate={this.state.validate}
                                    validationOption={{
                                        name: 'Quantity',
                                        check:true,
                                        required: true,
                                    }}
                                />
                        </div>
                    </div>
                    <div className="form-group">
                        <label className="control-label" htmlFor="minQty">
                            <span className="require">*</span>Minimum Quantity
                        </label>
                        <div className="col-sm-10">
                                <Textbox
                                    id='minQty'
                                    classNameInput="form-control"
                                    name="Min_Quantity"
                                    type="number"
                                    placeholder="Min Quantity"
                                    value = {this.context.productForm.minQty}
                                    onChange={(value) => this.props.handleInputChange(value,"minQty")}
                                    onBlur={() => { }}
                                    validate={this.state.validate}
                                    validationOption={{
                                        name: 'Min Quantity',
                                        check:true,
                                        required: true,
                                    }}
                                />
                        </div>
                    </div>
                    <div className="form-group">
                        <label className="control-label" htmlFor="price">Price</label>
                        <div className="col-sm-10">
                                <Textbox
                                    id='price'
                                    classNameInput="form-control"
                                    name="Price"
                                    type="number"
                                    placeholder="Price"
                                    value = {this.context.productForm.price}
                                    onChange={(value) => this.props.handleInputChange(value,"price")}
                                    validate={this.state.validate}
                                    disabled={this.context.productForm.isPriceOnDemand}
                                    validationOption={{
                                        name: 'Price',
                                        check:true,
                                        required: true,
                                    }}
                                />
                        </div>
                    </div>
                    <button type="submit" className="newsletter-btn col-sm-10"> Submit</button>
                </form>			
			</React.Fragment>
		);
	}
}
ManageProductForm.contextType = ManageProductContext;
export default ManageProductForm