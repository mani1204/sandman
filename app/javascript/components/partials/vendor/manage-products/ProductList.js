import React from 'react'
import ManageProductContext from 'utils/context/vendor/ManageProductContext'
import Product1Image from 'packs/images/1.jpg'
import Product2Image from 'packs/images/2.jpg'
import Product3Image from 'packs/images/3.jpg'
import Product4Image from 'packs/images/4.jpg'
import Product5Image from 'packs/images/5.jpg'

class ProductList extends React.Component {
	constructor(props) {
        super(props)
    }
    handleSideBarButtonClick = (product) => {
    	this.props.onSideBarButtonClick(product)
	}
	render(){
		return (
	        <div className="single-product"  >
	            <div className="pro-img">
	            	<img className="img" src={Product1Image} alt="product-image"/>	
	            </div>
	            <div className="pro-content" >
	                <div className="product-rating">
	                    <i className="fa fa-star"></i>
	                    <i className="fa fa-star"></i>
	                    <i className="fa fa-star"></i>
	                    <i className="fa fa-star"></i>
	                    <i className="fa fa-star"></i>
	                </div>
		            <h4 onClick={()=>this.handleSideBarButtonClick(this.props.product)}><a href="#">{this.props.product.name}</a></h4>
		            <p>
		            	<span className="price">{this.props.product.price}</span>
		            </p>
	            </div>
	        </div>
		);
	}
}
ProductList.contextType = ManageProductContext;
export default ProductList
