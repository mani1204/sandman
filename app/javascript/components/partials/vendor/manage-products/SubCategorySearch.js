import React from 'react'
import Select from 'react-select'
import ManageProductContext from 'utils/context/vendor/ManageProductContext'

class SubCategorySearch extends React.Component{
	render(){
		return(
			<div className="pt-40 ">
			    <div className="container">
			    	<Select
					   	options={this.context.subCategories}
						onChange = {(values)=>this.props.handleSubCategoryChange(values)}
					/>			    	
			 	</div>
			</div>
		)
	}
}
SubCategorySearch.contextType=ManageProductContext
export default SubCategorySearch