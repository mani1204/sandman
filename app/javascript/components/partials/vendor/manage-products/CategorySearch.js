import React from 'react'
import Select from 'react-select'
import ManageProductContext from 'utils/context/vendor/ManageProductContext'

class CategorySearch extends React.Component{
	render(){
		return(
			<div className="pt-40 pv-30" onClick={this.clearProducts}>
			    <div className="container">
					<Select
					   	options={this.context.categories}
				        onChange = {(values)=>this.props.handleCategoryChange(values)}
					 />
			 	</div>
			</div>
		)
	}
}
CategorySearch.contextType=ManageProductContext
export default CategorySearch