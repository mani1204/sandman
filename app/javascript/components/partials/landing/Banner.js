import React from 'react'

import Banner1Image from 'packs/images/1.png';
import Banner2Image from 'packs/images/2.png';

class Banner extends React.Component
{
	render()
	{
		return (
			<div className="upper-banner banner pb-60" style={this.props.outerStyle}>
	            <div className="container">
	               <div className="row">
	                   <div className="col-sm-6">
	                        <div className="single-banner zoom">
	                            <a href="#">
	                            	<img src={Banner1Image} alt="slider-banner"/>
	                            </a>
	                        </div>
	                    </div>
	                   <div className="col-sm-6">
	                        <div className="single-banner zoom">
	                            <a href="#">
	                            	<img src={Banner2Image} alt="slider-banner"/>
	                            </a>
	                        </div>
	                    </div>
	               </div>
	            </div>
	        </div>                                
		);
	}
}

export default Banner