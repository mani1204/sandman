import React from 'react'

import Product1Image from 'packs/images/1.jpg'
import Product2Image from 'packs/images/2.jpg'
class Product extends React.Component
{
    constructor(props)
    {
        super(props)
    }

    handleProductClick = (e) =>
    {
        window.location = '/products/' + this.props.product.id;
    }

    productStyle = {
        cursor: 'pointer',
    }

	render()
	{
		return (
			<div className={"single-product " + this.props.outerClass} onClick={this.handleProductClick} style={this.productStyle}>
                <div className="pro-img">
                    <img className="primary-img" src={Product1Image} alt="single-product"/>
                    <img className="secondary-img" src={Product2Image} alt="single-product"/>
                </div>
                <div className="pro-content">
                    <div className="product-rating">
                        <i className="fa fa-star"></i>
                        <i className="fa fa-star"></i>
                        <i className="fa fa-star"></i>
                        <i className="fa fa-star"></i>
                        <i className="fa fa-star"></i>
                    </div>                                
                    <h4>
                    	{this.props.product.name}
                    </h4>
                    <p>
                    	<span className="price">{this.props.product.price}</span>
                    	{ /*<del className="prev-price">$32.00</del> */ }
                    </p>
                    <div className="pro-actions">
                        <div className="actions-secondary">
                            <a href="wishlist.html" data-toggle="tooltip" title="Add to Wishlist">
                            	<i className="fa fa-heart"></i>
                            </a>
                            <a className="add-cart" href="cart.html" data-toggle="tooltip" title="Add to Cart">Add To Cart</a>
                            <a href="compare.html" data-toggle="tooltip" title="Add to Compare">
                            	<i className="fa fa-signal"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>             
		);
	}
}

export default Product