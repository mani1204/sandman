import React from 'react'

import FreeDeliveryImage from 'packs/images/free-delivery.png'
import OnlineSupportImage from 'packs/images/online-support.png'
import MoneyReturnImage from 'packs/images/money-return.png'
import MemberDiscountImage from 'packs/images/member-discount.png'

class CompanyPolicy extends React.Component
{
	render()
	{
		return (
			<div className="company-policy pb-60">
	            <div className="container">
	                <div className="row">
	                    <div className="col-lg-3 col-sm-6">
	                        <div className="single-policy">
	                            <div className="icone-img">
	                                <img src={FreeDeliveryImage} alt=""/>
	                            </div>
	                            <div className="policy-desc">
	                                <h3>Free Delivery</h3>
	                                <p>Free shipping on all order</p>
	                            </div>
	                        </div>
	                    </div>
	                    <div className="col-lg-3 col-sm-6">
	                        <div className="single-policy">
	                            <div className="icone-img">
	                                <img src={OnlineSupportImage} alt=""/>
	                            </div>
	                            <div className="policy-desc">
	                                <h3>Online Support 24/7</h3>
	                                <p>Support online 24 hours</p>
	                            </div>
	                        </div>
	                    </div>
	                    <div className="col-lg-3 col-sm-6">
	                        <div className="single-policy">
	                            <div className="icone-img">
	                                <img src={MoneyReturnImage} alt=""/>
	                            </div>
	                            <div className="policy-desc">
	                                <h3>Money Return</h3>
	                                <p>Back guarantee under 7 days</p>
	                            </div>
	                        </div>
	                    </div>
	                    <div className="col-lg-3 col-sm-6">
	                        <div className="single-policy">
	                            <div className="icone-img">
	                                <img src={MemberDiscountImage} alt=""/>
	                            </div>
	                              <div className="policy-desc">
	                                <h3>Member Discount</h3>
	                                <p>Onevery order over $30.00</p>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div>                           
		);
	}
}

export default CompanyPolicy