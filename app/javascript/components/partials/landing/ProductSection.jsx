import React from 'react'

import Product from './Product'
import ProductService from 'services/commerce/Product'
import flatten from 'lodash/flatten'
class ProductSection extends React.Component
{
	constructor(props)
	{
		super(props)
		this.state = {
			categories: [],
			products: [],
			selected_category_id: null
		}
	}

	componentDidMount()
	{
		ProductService.getProducts({}).then((r) => {
			this.setState({
					categories: r.data.categories,
					products: flatten(r.data.products),
					selected_category_id: r.data.selected_category_id
				})
		})
		.catch((e) => {
			console.log(e)
		})
		.then(() => {
			console.log("Completed")
		})
	}	

	clickCategory(categoryId, event)
	{
		event.preventDefault();
		ProductService.getProducts({category_id: categoryId}).then((r) => {
			this.setState({
					categories: r.data.categories,
					products: flatten(r.data.products),
					selected_category_id: r.data.selected_category_id
				})
		})
		.catch((e) => {
			console.log(e)
		})
		.then(() => {
			console.log("Completed")
		})
	}

	getCategories()
	{
		let categories = []
		this.state.categories.forEach((c) =>{
			categories.push(
				<li key={c.id}>
		        	<a className={c.id == this.state.selected_category_id ? 'active': ''} data-toggle="tab" href="#newArrival" onClick={(e) => this.clickCategory(c.id, e)}>
		        		{c.name}
		        	</a>
		        </li>
			)
		})

		return categories
	}

	getProducts()
	{
		let products = []
		this.state.products.forEach((p) => {
				products.push(
					<Product key={p.id} product={p} outerClass="col-md-3"/>
				)
			})
		return products
	}

	render()
	{
		return (
			<div className="new-products pb-60">
	            <div className="container">
	                <div className="row">
	                    <div className="col-xl-12 col-lg-12  order-lg-2">
	                        <div className="new-pro-content">
	                            <div className="pro-tab-title border-line">
	                                <ul className="nav product-list product-tab-list">
	                                	{this.getCategories()}
	                                </ul>
	                            </div>
	                            <div className="tab-content product-tab-content jump">
	                                <div id="newArrival" className="tab-pane active">
	                                	<div className="row">
	                                		{this.getProducts()}
	                                   	</div>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div>
		);
	}
}

export default ProductSection