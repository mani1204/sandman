import React from 'react'
import Product1Image from 'packs/images/1(4).png'

class BrandSection extends React.Component
{
	render()
	{
		return (
			<div className="brand-area pb-60">
	            <div className="container">
	                	<div className="row m-0">
		                	{
		                		Array(10).fill('').map((x,i) => {
		                			return (
		                				<div key={i.toString()} className="single-brand col-md-3 text-center p-15 border">
					                        <a href="#">
					                        	<img className="img" src={Product1Image} alt="brand-image"/>
					                        </a>
					                    </div>
		                			)
		                		})	
		                	}
	                		
	                	</div>
	            </div>
	        </div>
		);
	}
}

export default BrandSection