import React from "react"

import Product5 from 'packs/images/5.jpg'
import Product6 from 'packs/images/6.jpg'
import Product9 from 'packs/images/9.jpg'
import Product10 from 'packs/images/10.jpg'
import ErrorBoundary from "../../ErrorBoundary"
class Slider extends React.Component
{
	componentDidMount()
	{
	    $('#slider').nivoSlider({
	        effect: 'random',
	        animSpeed: 300,
	        pauseTime: 5000,
	        directionNav: false,
	        manualAdvance: true,
	        controlNavThumbs: false,
	        pauseOnHover: true,
	        controlNav: true,
	        prevText: "<i class='zmdi zmdi-chevron-left'></i>",
	        nextText: "<i class='zmdi zmdi-chevron-right'></i>"
	    });
	}

	render()
	{
		return(
				<ErrorBoundary>
					<div className="slider-area slider-style-three">
			            <div className="container">
			                <div className="row">
			                    <div className="col-md-8">
			                        <div className="slider-wrapper theme-default">
			                            <div id="slider" className="nivoSlider">
			                                <a href="shop.html"> 
			                                	<img src={Product5} data-thumb="img/slider/5.jpg" alt="" title="#slider-1-caption1"/>
			                                </a>
			                                <a href="shop.html">
			                                	<img src={Product6} data-thumb="img/slider/6.jpg" alt="" title="#slider-1-caption2"/>
			                                </a>
			                            </div>
			                            <div id="slider-1-caption1" className="nivo-html-caption nivo-caption">
			                                <div className="text-content-wrapper">
			                                    <div className="text-content">
			                                        <h4 className="title2 wow bounceInLeft text-white mb-16" data-wow-duration="2s" data-wow-delay="0s">Big Sale</h4>
			                                        <h1 className="title1 wow bounceInRight text-white mb-16" data-wow-duration="2s" data-wow-delay="1s">Hand Tools <br/>Power Saw Machine</h1>
			                                        <div className="banner-readmore wow bounceInUp mt-35" data-wow-duration="2s" data-wow-delay="2s">
			                                            <a className="button slider-btn" href="shop.html">Shop Now</a>                    
			                                        </div>
			                                    </div>
			                                </div>
			                            </div>  
			                            <div id="slider-1-caption2" className="nivo-html-caption nivo-caption">
			                                <div className="text-content-wrapper">
			                                    <div className="text-content slide-2">
			                                        <h4 className="title2 wow bounceInLeft text-white mb-16" data-wow-duration="1s" data-wow-delay="1s">Big Sale</h4>
			                                        <h1 className="title1 wow flipInX text-white mb-16" data-wow-duration="1s" data-wow-delay="2s">Hand Tools <br/>Power Saw Machine</h1>
			                                        <div className="banner-readmore wow bounceInUp mt-35" data-wow-duration="1s" data-wow-delay="3s">
			                                            <a className="button slider-btn" href="shop.html">Shop Now</a>                    
			                                        </div>
			                                    </div>       
			                                </div>
			                            </div>   
			                        </div>
			                    </div>
			                    <div className="col-md-4">
			                        <div className="single-banner zoom mb-20">
			                            <a href="#"><img src={Product9} alt="slider-banner"/></a>
			                        </div>
			                        <div className="single-banner zoom">
			                            <a href="#"><img src={Product10} alt="slider-banner"/></a>
			                        </div>
			                    </div>
			                </div>
			            </div>
			        </div>
	        	</ErrorBoundary>
		);
	}
}

export default Slider