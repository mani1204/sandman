import React, { Component } from 'react'
import about from '../../../packs/images/about.jpg'
class AboutUs extends Component {
    render() {
        return (
            <div className="whole-div">
                <div className="about-main-area">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-7 col-md-12">
                                <div className="about-img">
                                    <img className="img" src={about} alt="about-us" />
                                </div>
                            </div>
                            <div className="col-lg-5 col-md-12">
                                <div className="about-content">
                                    <h3>Why We are?</h3>
                                    <p>Mellentesque faucibus dapibus dapibus. Morbi aliquam aliquet neque. Donec placerat dapibus sollicitudin.
                                        Morbi arcu nisi, mattis ullamcorper in, dapibus ac nunc. Cras bibendum mauris et sapien nibh feugiat.
                                      scelerisque accumsan nibh gravida. Quisque aliquet justo elementum lectus ultrices bibendum.</p><br />
                                    <p>dapibus ac nunc. Cras bibendum mauris et sapien feugiat. scelerisque accumsan nibh gravida. Quisque aliquet justo elementum lectus ultrices bibendum.</p>
                                    <ul className="mt-20 about-content-list">
                                        <li><a href="#">Amazing wordpress theme</a></li>
                                        <li><a href="#">HTML &amp; CSS3 build with bootstrap</a></li>
                                        <li><a href="#">Powerfull admin panel</a></li>
                                        <li><a href="#">Icon well organized &amp; SEO optimized friendy</a></li>
                                        <li><a href="#">Iconncredible design</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="about-bottom pt-50 pb-60">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-6 col-md-6">
                                <div className="ht-single-about pb-sm-40">
                                    <h3>OUR EXPERIENCES</h3>
                                    <h5>fusce fringilla porttitor iaculi sed quam libero, adipiscing sed erat id praesent eu nis.</h5>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent eu nisi ac mi malesuada vestibulum. Phasellus tempor nunc eleifend cursus molestie. Mauris lectus arcu, pellentesque at sodales sit amet, Phasellus tempor nunc eleifend cursus molestie. Mauris lectus arcu, pellentesque at sodales sit amet,</p>
                                    <p>Donec ornare mattis suscipit. Praesent fermentum accumsan vulputate. Sed velit nulla, sagittis non erat id, dictum vestibulum ligula. Maecenas sed enim sem.</p>
                                </div>
                            </div>
                            <div className="col-lg-6 col-md-6">
                                <div className="ht-single-about">
                                    <h3>OUR WORKS</h3>
                                    <div className="ht-about-work">
                                        <span>1</span>
                                        <div className="ht-work-text">
                                            <h5><a href="#">LOREM IPSUM DOLOR SIT AMET</a></h5>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent eu nisi ac mi</p>
                                        </div>
                                    </div>
                                    <div className="ht-about-work">
                                        <span>2</span>
                                        <div className="ht-work-text">
                                            <h5><a href="#">DONEC FERMENTUM EROS</a></h5>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent eu nisi ac mi</p>
                                        </div>
                                    </div>
                                    <div className="ht-about-work">
                                        <span>3</span>
                                        <div className="ht-work-text">
                                            <h5><a href="#">LOREM IPSUM DOLOR SIT AMET</a></h5>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent eu nisi ac mi</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default AboutUs