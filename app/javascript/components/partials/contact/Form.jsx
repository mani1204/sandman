import React, { Component } from 'react'
import axios from 'axios';
import validator from 'validator';
import swal from 'sweetalert';
import { Textbox, Textarea } from 'react-inputs-validation';
class Form extends Component {
    constructor(props) {

        super(props)
        this.state = {
            name: "",
            mobile: "",
            email: "",
            message: "",
            nameError: true,
            mobileError: true,
            emailError: true,
            messageError: true,
            validate: false
        }
        this.validateForm = this.validateForm.bind(this);
    }
    componentDidMount() {
        $.scrollUp({
            scrollName: 'scrollUp', // Element ID
            topDistance: '550', // Distance from top before showing element (px)
            topSpeed: 1000, // Speed back to top (ms)
            animation: 'fade', // Fade, slide, none
            scrollSpeed: 900,
            animationInSpeed: 1000, // Animation in speed (ms)
            animationOutSpeed: 1000, // Animation out speed (ms)
            scrollText: '<i class="far fa-angle-up fa-lg"></i>', // Text for element
            activeOverlay: false // Set CSS color to display scrollUp active point, e.g '#00FFFF'
        });
    }
    toggleValidating(validate) {
        this.setState({ validate });
    }
    validateForm = (e) => {
        e.preventDefault()
        this.toggleValidating(true);
        const {
            nameError,
            mobileError,
            emailError,
            messageError,
        } = this.state;
        if (!nameError && !mobileError && !emailError && !messageError) {
            const form = {
                name: this.state.name,
                email: this.state.email,
                phone: this.state.mobile,
                message: this.state.message
            }
            axios.post(`/api/v1/contact-us`, { contact_us: form }, {
                headers: {
                    'Content-Type': 'application/json',
                    'X-CSRF-Token': document.getElementsByName('csrf-token')[0].content
                }
            })
                .then(res => {
                    if (res.data.success) {
                        this.setState({ name: "", mobile: "", email: "", message: "" })
                        swal({
                            title: "Thank You",
                            text: "Thanks for Contacting Us. We will get back to you soon ",
                            icon: "success",
                            button: "Ok",
                          })
                    }
                    else {
                    }
                })
                .catch(e => {
                })
        }
    }
    render() {
        const { name, email, mobile, message, validate } = this.state;
        return (
            <div className="contact-email-area ptb-24">
                <div className="container">
                    <div className="row">
                        <div className="col-sm-12">
                            <legend className="contact-form-title" >Contact Us</legend>
                            <p className="text-capitalize mb-15">Lorem Ipsum Dolor Sit Amet, Consectetur Adipisicing Elit.</p>
                            <form onSubmit={this.validateForm} id="contact-form" className="contact-form" method="post">
                                <div className="address-wrappers">
                                    <div className="row ">
                                        <div className="col-md-12 bottom-space">
                                            <div className="address-fname">
                                                <Textbox
                                                    tabIndex="1"
                                                    id={'name'}
                                                    name="name"
                                                    type="text"
                                                    validate={validate}
                                                    validationCallback={res => this.setState({ nameError: res, validate: false })}
                                                    placeholder="Name"
                                                    maxLength={20}
                                                    value={this.state.name}
                                                    onChange={(name, e) => {
                                                        this.setState({ name });
                                                    }}
                                                    onBlur={() => { }}
                                                    validationOption={{
                                                        name: 'Name',
                                                        check: true,
                                                        required: true,
                                                        customFunc: name => {
                                                            const reg = /^(([a-zA-Z]))+$/;
                                                            if (reg.test(String(name))) {
                                                                return true;
                                                            } else {
                                                                return 'Name field only alphabets ';
                                                            }
                                                        },
                                                        min: 3,
                                                        max: 20,
                                                        type: 'string',
                                                        invalidFormat: name => `${name} is not a number(custom message)`,
                                                        invalid: name => `${name} invalid format(custom message)`,
                                                        locale: 'en-US',
                                                    }}
                                                />
                                            </div>
                                        </div>
                                        <div className="col-md-6 col-xs-12 bottom-space">
                                            <div className="address-email">
                                                <Textbox
                                                    tabIndex="1"
                                                    id={'email'}
                                                    name="email"
                                                    type="text"
                                                    maxLength={50}
                                                    validate={validate}
                                                    validationCallback={res => this.setState({ emailError: res, validate: false })}
                                                    placeholder="Email"
                                                    value={this.state.email}
                                                    onChange={(email, e) => {
                                                        this.setState({ email });
                                                    }}
                                                    onBlur={() => { }}
                                                    validationOption={{
                                                        name: 'Email',
                                                        check: true,
                                                        required: true,
                                                        customFunc: email => {
                                                            const reg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                                                            if (reg.test(String(email).toLowerCase())) {
                                                                return true;
                                                            } else {
                                                                return 'Please enter a valid email address';
                                                            }
                                                        }
                                                    }}
                                                />
                                            </div>
                                        </div>
                                        <div className="col-md-6 col-xs-12 bottom-space-responsive">
                                            <div className="address-mobile">
                                                <Textbox
                                                    tabIndex="1"
                                                    id={'mobile'}
                                                    name="mobile"
                                                    type="text"
                                                    maxLength={10}
                                                    validate={validate}
                                                    validationCallback={res => this.setState({ mobileError: res, validate: false })}
                                                    placeholder="Mobile"
                                                    value={this.state.mobile}
                                                    onChange={(mobile, e) => {
                                                        this.setState({ mobile });
                                                    }}
                                                    onBlur={() => { }}
                                                    validationOption={{
                                                        name: 'Mobile',
                                                        check: true,
                                                        required: true,
                                                        type: "number",
                                                        length: 10,
                                                        min: 10,
                                                        customFunc: phone => {
                                                            if (validator.isMobilePhone(phone, 'en-IN')) {
                                                                return true;
                                                            }
                                                            else {
                                                                return 'Please enter a valid phone number';
                                                            }
                                                        }
                                                    }}
                                                />
                                            </div>
                                        </div>
                                        <div className="col-12">
                                            <div className="address-textareas">
                                                <Textarea
                                                    tabIndex="1"
                                                    id={'description'}
                                                    name="message"
                                                    type="text"
                                                    maxLength={150}
                                                    validate={validate}
                                                    validationCallback={res => this.setState({ messageError: res, validate: false })}
                                                    placeholder="Message"
                                                    value={this.state.message}
                                                    onChange={(message, e) => {
                                                        this.setState({ message });
                                                    }}
                                                    onBlur={() => { }}
                                                    validationOption={{
                                                        name: 'Message',
                                                        check: true,
                                                        required: true,
                                                        min: 30,
                                                        max: 150,
                                                        required: true,
                                                    }}
                                                />
                                            </div>
                                        </div>
                                        <div className="col-12 footer-content mail-content">
                                            <div className="send-email">
                                                <button className="submit">Submit</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
export default Form;



