import React from 'react';
import ReactDOM from 'react-dom';

class Sidebar extends React.Component 
{
	render()
	{
		return (
			<div className="col-lg-2">
				<ul className="nav flex-column dashboard-list" role="tablist">
					<li><a onClick={() => this.props.onClick('home')} className={this.active('home')} data-toggle="tab" href="#dashboard">Dashboard</a></li>
					<li><a onClick={() => this.props.onClick('orders')} className={this.active('orders')} data-toggle="tab" href="#orders">Orders</a></li>
					<li><a onClick={() => this.props.onClick('addresses')} className={this.active('addresses')} data-toggle="tab" href="#address">Addresses</a></li>
					<li><a onClick={() => this.props.onClick('accounts')} className={this.active('accounts')} data-toggle="tab" href="#account-details">Account details</a></li>
					<li><a onClick={() => this.props.onClick('logout')} className={this.active('logout')}  href="login.html" href="#logout">logout</a></li>
				</ul>
			</div>
		);
	}

	active = (topic) => {
		return (this.props.currentPage == topic) ? 'active' : ''
	}
}
export default Sidebar