import React from 'react';
import ReactDOM from 'react-dom';

class Downloads extends React.Component 
{
	render()
	{
		return (
			<div id="downloads" className="tab-pane active">
				<h3>Downloads</h3>
				<div className="table-responsive">
					<table className="table">
						<thead>
							<tr>
								<th>Product</th>
								<th>Downloads</th>
								<th>Expires</th>
								<th>Download</th>	 	 	 	
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>Proecut Name Here</td>
								<td>May 10, 2018</td>
								<td>never</td>
								<td><a className="view" href="#">Click Here To Download Your File</a></td>
							</tr>
							<tr>
								<td>Proecut Name Here</td>
								<td>Sep 11, 2018</td>
								<td>never</td>
								<td><a className="view" href="#">Click Here To Download Your File</a></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		);
	}
}
export default Downloads