import React from 'react';
import ReactDOM from 'react-dom';
import Sidebar from "components/partials/customer/sidebar"
import Home from "components/partials/customer/Home"
import Orders from "components/partials/customer/orders"
import Address from "components/partials/customer/Address"
import Account from "components/partials/customer/account"


class Dashboard extends React.Component 
{
	constructor(props){
		super(props)
		this.state = {
			currentPage: this.observeURL()
		}
	}

	observeURL(){
		const url = window.location.href
		const topic = url.split('#')[1]
		const splits = ["home" , "orders" , "addresses" , "accounts"]
		if(splits.indexOf(topic) > -1)
			return topic
		else 
			return 'home'
	}

	handleSideBarButtonClick = (topic) => {
		this.setState({
			currentPage: topic
		})
	}

	_renderPageSection = () =>
	{
		switch(this.state.currentPage){
			case 'home':
				return <Home />
				break;
			case 'orders':
				return <Orders />
				break;
			case 'addresses':
				return <Address />
				break;
			case 'accounts':
				return <Account />
				break;
			default:
				break;
		}
	}
	render()
	{
		return (
			<div className="my-account white-bg pb-60">
				<div className="container">
					<div className="row">
						<Sidebar currentPage={this.state.currentPage} onClick={this.handleSideBarButtonClick}/>
						<div className="col-lg-10">
							<div className="tab-content dashboard-content mt-all-40">
								<div>
									{this._renderPageSection()}
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default Dashboard;
