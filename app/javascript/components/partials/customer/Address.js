import React from 'react';
import ReactDOM from 'react-dom';

class Address extends React.Component 
{
	render()
	{
		return (
			<div id="address" className="tab-pane active">
				<p>The following addresses will be used on the checkout page by default.</p>
				<h4 className="billing-address">Billing address</h4>
				<a className="view" href="#">edit</a>
				<p>steven smith</p>
				<p>Australia</p>   
			</div>
		);
	}
}
export default Address