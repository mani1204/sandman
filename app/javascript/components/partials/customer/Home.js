import React from 'react';
import ReactDOM from 'react-dom';

class Home extends React.Component 
{
	render()
	{
		return (
			<div id="dashboard" className="tab-pane active">
				<h3>Dashboard </h3>
				<p>From your account dashboard. you can easily check & view your <a href="#">recent orders</a>, manage your <a href="#">shipping and billing addresses</a> and <a href="#">edit your password and account details.</a></p>
			</div>	
		);
	}
}
export default Home