import React from 'react';
import ReactDOM from 'react-dom';

class Account extends React.Component 
{
	render()
	{
		return (
			<div id="account-details" className="tab-pane active">
				<h3>Account details </h3>
				<div className="register-form login-form clearfix">
					<form action="#">
						<div className="form-group row align-items-center">
							<label className="col-lg-3 col-md-4 col-form-label">Social title</label>
							<div className="col-lg-6 col-md-8">
								<span className="custom-radio"><input name="id_gender" value="1" type="radio" /> Mr.</span>
								<span className="custom-radio"><input name="id_gender" value="1" type="radio" /> Mrs.</span>
							</div>
						</div>
						<div className="form-group row">
							<label htmlFor="f-name" className="col-lg-3 col-md-4 col-form-label">First Name</label>
							<div className="col-lg-6 col-md-8">
								<input type="text" className="form-control" id="f-name" />
							</div>
						</div>
						<div className="form-group row">
							<label htmlFor="l-name" className="col-lg-3 col-md-4 col-form-label">Last Name</label>
							<div className="col-lg-6 col-md-8">
								<input type="text" className="form-control" id="l-name"/>
							</div>
						</div>
						<div className="form-group row">
							<label htmlFor="email" className="col-lg-3 col-md-4 col-form-label">Email address</label>
							<div className="col-lg-6 col-md-8">
								<input type="text" className="form-control" id="email"/>
							</div>
						</div>
						<div className="form-group row">
							<label htmlFor="inputpassword" className="col-lg-3 col-md-4 col-form-label">Current password</label>
							<div className="col-lg-6 col-md-8">
								<input type="password" className="form-control" id="inputpassword" />
							</div>
						</div>
						<div className="form-group row">
							<label htmlFor="newpassword" className="col-lg-3 col-md-4 col-form-label">New password</label>
							<div className="col-lg-6 col-md-8">
								<input type="password" className="form-control" id="newpassword" />
								<button className="btn show-btn" type="button">Show</button>
							</div>
						</div>
						<div className="form-group row">
							<label htmlFor="v-password" className="col-lg-3 col-md-4 col-form-label">Confirm password</label>
							<div className="col-lg-6 col-md-8">
								<input type="password" className="form-control" id="c-password" />
								<button className="btn show-btn" type="button">Show</button>
							</div>
						</div>
						<div className="form-group row">
							<label htmlFor="birth" className="col-lg-3 col-md-4 col-form-label">Birthdate</label>
							<div className="col-lg-6 col-md-8">
								<input type="text" className="form-control" id="birth" placeholder="MM/DD/YYYY" />
							</div>
						</div>
						<div className="form-check row p-0 mt-20">
							<div className="col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-4">
								<input className="form-check-input" value="#" id="offer" type="checkbox" />
								<label className="form-check-label" htmlFor="offer">Receive offers from our partners</label>
							</div>
						</div>
						<div className="form-check row p-0 mt-20">
							<div className="col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-4">
								<input className="form-check-input" value="#" id="subscribe1" type="checkbox" />
								<label className="form-check-label" htmlFor="subscribe">Sign up for our newsletter<br />Subscribe to our newsletters now and stay up-to-date with new collections, the latest lookbooks and exclusive offers..</label>
							</div>
						</div>
						<div className="register-box mt-40">
							<button type="submit" className="return-customer-btn f-right">Save</button>
						</div>
					</form>
				</div>
			</div>
		);
	}
}
export default Account