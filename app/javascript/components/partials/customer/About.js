import React from 'react';
import ReactDOM from 'react-dom';
import Cart from "components/common/Cart"


class About extends React.Component {
render(){
 return (
 	<div className="my-account white-bg pb-60">
      <div className="container">
        <div className="account-dashboard">
          <div className="dashboard-upper-info">
            <div className="row no-gutters align-items-center">
              <div className="col-lg-3 col-md-6">
                <div className="d-single-info">
                  <p className="user-name">Hello <span>yourmail@info</span></p>
                  <p>(not yourmail@info? <a href="#">Log Out</a>)</p>
                </div>
              </div>

              <div className="col-lg-3 col-md-6">
                <div className="d-single-info">
                  <p>Need Assistance? Customer service at.</p>
                  <p>admin@example.com.</p>
                </div>
              </div>

              <div className="col-lg-3 col-md-6">
                <div className="d-single-info">
                  <p>E-mail them at </p>
                  <p>support@example.com</p>
                </div>
              </div>
              
              <div className="col-lg-3 col-md-6">
                <div className="d-single-info text-center">
                  <a className="view-cart" href="cart.html"><i className="fa fa-cart-plus" aria-hidden="true"></i> view cart</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    );
}
}

export default About;