import React from 'react'
import Product1Image from 'packs/images/1.jpg'
import Product2Image from 'packs/images/2.jpg'
import ProductContext from "utils/context/ProductContext"
import ImageSection from './ImageSection'
import DetailSection from './DetailSection'
class Index extends React.Component{
	render(){
		return (
			<div className="main-product-thumbnail pb-60">
	            <div className="container">
	                <div className="row">
	                    <div className="col-lg-5">
	                    	<ImageSection />
	                    </div>
	                    <div className="col-lg-7">
	                        <DetailSection/>
	                    </div>
	                </div>
	            </div>
	        </div>
		)
	}
}
Index.contextType = ProductContext;

export default Index