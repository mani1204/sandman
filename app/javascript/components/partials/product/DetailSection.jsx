import React from 'react'
import ProductContext from "utils/context/ProductContext"
class DetailSection extends React.Component {
    render() {
        return ( 
            <div className="thubnail-desc fix">
                <h3 className="product-header">{this.context.product.name}</h3>
                {/*
                    <div className="rating-summary fix mtb-10">
                        <div className="rating f-left">
                            <i className="fa fa-star"></i>
                            <i className="fa fa-star"></i>
                            <i className="fa fa-star-o"></i>
                            <i className="fa fa-star-o"></i>
                            <i className="fa fa-star-o"></i>
                        </div>
                        
                        <div className="rating-feedback f-left">
                            <a href="#">(1 review)</a>
                            <a href="#">add to your review</a>
                        </div>
                    </div>
                */}
                <div className="pro-price mb-10">
                    <p>
                        <span className="price">Rs. {this.context.product.price}</span>
                    </p>
                </div>
                <div className="pro-ref mb-15">
                    <p>
                        <span className="in-stock">IN STOCK</span>
                    </p>
                </div>
                <div className="box-quantity">
                    <input className="number" type="number" min="1" value={this.context.qty} onChange={this.context.onQuantityChange} />
                    {this.context.cart === null ? <a className="add-cart" href="#" onClick={this.context.onAddToCartClick}>add to cart</a>
                        : <div className="d-inline">
                            {this.context.cart.qty == this.context.qty ?
                                <a className="add-cart disable-cart">Update cart</a>
                                :
                                <a className="add-cart" href="#" onClick={this.context.onUpdateToCartClick}>Update cart</a>
                            }
                            <a className="add-cart ml-15" href="#" onClick={this.context.onDeleteFromCartClick}>Delete cart</a>
                        </div>
                    }
                </div>
                <div className="product-link">
                    <ul className="list-inline">
                        <li><a className="mr-3" href="wishlist.html">Add to Wish List</a></li>
                        <li><a className="mr-3" href="compare.html">Add to compare</a></li>
                        <li><a className="mr-3" href="#">Email</a></li>
                    </ul>
                </div>
                <p className="ptb-20">Everything you need for a trip to the gym will fit inside this surprisingly spacious Products Name Here. Stock it with a water bottle, change of clothes, pair of shoes, and even a few beauty products. Fits inside a locker and zips shut for security.</p>
            </div>
        )
    }
}
DetailSection.contextType = ProductContext;
export default DetailSection