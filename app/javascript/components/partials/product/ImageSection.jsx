import React from 'react'

import Product1Image from 'packs/images/1.jpg'
import Product2Image from 'packs/images/2.jpg'
import Product3Image from 'packs/images/3.jpg'
import Product4Image from 'packs/images/4.jpg'

import ProductContext from "utils/context/ProductContext"
class ImageSection extends React.Component{
	render(){
		return (
			<React.Fragment>
				<div className="tab-content">
	                <div id="thumb1" className="tab-pane active">
	                    <a data-fancybox="images" href="">
	                    	<img src={Product1Image} alt="product-thumbnail"/>
	                    </a>
	                </div>
	                <div id="thumb2" className="tab-pane">
	                    <a data-fancybox="images" href="">
	                    	<img src={Product2Image} alt="product-thumbnail"/>
	                    </a>
	                </div>
	                <div id="thumb3" className="tab-pane">
	                    <a data-fancybox="images" href="">
	                    	<img src={Product3Image} alt="product-thumbnail"/>
	                    </a>
	                </div>
	                <div id="thumb4" className="tab-pane">
	                    <a data-fancybox="images" href="">
	                    	<img src={Product4Image} alt="product-thumbnail"/>
	                    </a>
	                </div>
	            </div>
	            <div className="product-thumbnail">
	                <div className="thumb-menu nav">
	                        <a className="active" data-toggle="tab" href="#thumb1"> 
	                        	<img src={Product1Image} alt="product-thumbnail"/>
	                        </a>
	                        <a data-toggle="tab" href="#thumb2"> 
	                        	<img src={Product2Image} alt="product-thumbnail"/>
	                        </a>
	                        <a data-toggle="tab" href="#thumb3"> 
	                        	<img src={Product3Image} alt="product-thumbnail"/>
	                        </a>
	                        <a data-toggle="tab" href="#thumb4"> 
	                        	<img src={Product4Image} alt="product-thumbnail"/>
	                        </a>
	                </div>
	            </div>
            </React.Fragment>
		)
	}
}

export default ImageSection