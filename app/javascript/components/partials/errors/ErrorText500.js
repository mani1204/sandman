import React from 'react';
import ReactDOM from 'react-dom';


class ErrorText500 extends React.Component {
	render(){
		return (
			<div className="error-text">
				<h1>500</h1>
				<h2>Oops! Something Went Wrong</h2>
				<p>Try to Refresh this page or feel free to contact us if the problem persits.</p>
			</div>
		);
	}
}

export default ErrorText500