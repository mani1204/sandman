import React from 'react';
import ReactDOM from 'react-dom';
import ErrorText404 from "components/partials/errors/ErrorText404";
import Search from "components/partials/errors/Search";
import ErrorButton from "components/partials/errors/ErrorButton";


class Error404 extends React.Component {
    render(){
        return (
            <div className="error404-area pb-60 pt-20">
                <div className="container">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="error-wrapper text-center">
                                <ErrorText404 />
                                <Search />
                                <ErrorButton />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
 );
}
}

export default Error404;