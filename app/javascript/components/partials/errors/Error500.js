import React from 'react';
import ReactDOM from 'react-dom';
import ErrorText500 from "components/partials/errors/ErrorText500";
import Search from "components/partials/errors/Search";
import ErrorButton from "components/partials/errors/ErrorButton";


class Error500 extends React.Component {
    render(){
        return (
            <div className="error404-area pb-60 pt-20">
                <div className="container">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="error-wrapper text-center">
                                <ErrorText500 />
                                <Search />
                                <ErrorButton />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
 );
}
}

export default Error500;