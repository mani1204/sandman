import React from 'react';
import ReactDOM from 'react-dom';
import Cart from "components/common/Cart"


class Search extends React.Component {
	render(){
		return (
 			<div className="search-error">
				<form id="search-form" action="#">
					<input type="text" placeholder="Search" />
					<button><i className="fa fa-search"></i></button>
				</form>
			</div>
		);
	}
}

export default Search