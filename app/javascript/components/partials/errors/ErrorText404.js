import React from 'react';
import ReactDOM from 'react-dom';


class ErrorText404 extends React.Component {
	render(){
		return (
			<div className="error-text">
				<h1>404</h1>
				<h2>Oops! PAGE NOT BE FOUND</h2>
				<p>Sorry but the page you are looking for does not exist, have been removed, name changed or is temporarily unavailable.</p>
			</div>
		);
	}
}

export default ErrorText404