import React from 'react'
import image1 from 'packs/images/1.jpg'
import image2 from 'packs/images/2.jpg'
import image3 from 'packs/images/5.jpg'
import image4 from 'packs/images/4.jpg'
class Index extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			wishlist: [
				{
					id: 0,
					image: image1,
					product: "product1",
					price: 150,
					quantity: 0,
				},
				{
					id: 1,
					image: image2,
					product: "product1",
					price: 150,
					quantity: 0,
				},
				{
					id: 2,
					image: image3,
					product: "product1",
					price: 150,
					quantity: 0,
				},
				{
					id: 3,
					image: image4,
					product: "product1",
					price: 150,
					quantity: 0,
				},
			]
		}
	}
	clickDeleteIcon = (itemId) => {
		const afterDelete = this.state.wishlist.filter((item) => item.id !== itemId);
		this.setState({ wishlist: afterDelete })
	}
	render() {
		return (
			<div className="container">
				<div className="cart-main-area wish-list pb-60">
					<h2 className="text-capitalize sub-heading">Wishlist</h2>
					<div className="row">
						<div className="col-md-12">
							<form action="#">
								<div className="table-content table-responsive">
									<table>
										<thead>
											<tr>
												<th className="product-remove">Remove</th>
												<th className="product-thumbnail">Image</th>
												<th className="product-name">Product</th>
												<th className="product-price">Unit Price</th>
												<th className="product-quantity">Stock Status</th>
												<th className="product-subtotal">add to cart</th>
											</tr>
										</thead>
										<tbody>
											{this.state.wishlist.length <= 0 ? <tr><td className="empty-cart" colSpan="6">No items in the WishList</td></tr> : this.state.wishlist.map((item, index) => (
												<tr key={item.id}>
													<td className="product-remove">
														<p onClick={() => this.clickDeleteIcon(item.id)}><a href="#"><i className="fa fa-times" aria-hidden="true"></i></a></p>
													</td>
													<td className="product-thumbnail">
														<a href="#"><img src={item.image} alt="wishlist-image" /></a>
													</td>
													<td className="product-name">
														<a href="#">{item.product}</a>
													</td>
													<td className="product-price"><span className="amount">{item.price}</span></td>
													<td className="product-stock-status"><span>in stock</span></td>
													<td className="product-add-to-cart"><a href="#">add to cart</a></td>
												</tr>
											))}
										</tbody>
									</table>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		);
	}
}
export default Index
