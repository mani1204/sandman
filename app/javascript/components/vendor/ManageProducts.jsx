import React from "react"
import Header from "./common/Header"
import Footer from "./common/Footer"
import CategorySearch from '../partials/vendor/manage-products/CategorySearch'
import SubCategorySearch from '../partials/vendor/manage-products/SubCategorySearch'
import ManageProductForm from '../partials/vendor/manage-products/ManageProductForm'
import ProductList from '../partials/vendor/manage-products/ProductList'
import ManageProductContext from 'utils/context/vendor/ManageProductContext'
import axios from 'axios'
import filter from "lodash/filter"
import OwlCarousel from 'react-owl-carousel2'

class ManageProducts extends React.Component
{
	constructor(props)
	{
		super(props)
		this.state = {
			allCategories: [],
			categories: [],
			subCategories: [],
			selectedCategoryId: null,
			selectedSubCategoryId: null,
			products: [],
			selectedProduct:"",
			productForm: {
				quantity: "",
				minQty: "",
				price: "",
				isPriceOnDemand:false,
			},
		}
	}

	handleInputChange = (value,name,isPriceOnDemand = true) =>{ 
     	this.setState({productForm: { ...this.state.productForm, ...{ [name] : value } } });
 	}

	componentDidMount(){
		this.fetchCategories();		
	}	

	fetchCategories(){
		axios.get('/api/v1/vendors/categories')
		.then((r) => {
			this.setState({
				allCategories: r.data.categories,
			})
			{
				let category = filter(this.state.allCategories,{'category_id': null})
				let categories = []
				category.map((c) =>{
					const value = {value: c.id, label: c.name}
					categories.push(value)	        
				})
				this.setState({categories:categories})
			}				
		})
		.catch((e) => {
			console.log(e)
		})			
	}

	fetchProducts(categoryId){
		axios.get('/api/v1/vendors/product_list', {
			params:{
						category_id: categoryId
					}
		})
		.then((r) => {
			this.setState({
				products: r.data.products,
			})
		})
		.catch((e) => {
			console.log(e)
		})
	}

	handleCategoryChange=(value)=>{
		let a = filter(this.state.allCategories,{'category_id': value.value})
		let subCategories=[]
		a.map((c) =>{
			const value = {value: c.id, label: c.name}
			subCategories.push(value)
		})
		this.setState({	
			subCategories:subCategories,
			selectedCategoryId: value.value,
		})
		this.fetchProducts(value.value)
	}

	handleSubCategoryChange(value){
		this.setState({
			selectedSubCategoryId: value.value
		})
		this.fetchProducts(value.value)
	}

 	getSideBarButtonClick = (product) => {
		this.setState({
			selectedProduct: product,
		})
		this.clearForm()
	}

	clearForm = () => {
		this.setState({
			productForm:{
				quantity: "",
				minQty: "",
				price: "",
				isPriceOnDemand:false,
			}
		})
	}

 	_renderProducts = () => {
		let groupedProducts=[]
		if(this.state.products.length > 0)
		{
			const pages = this.state.products.length/5
			for(let page = 0; page<pages ;page++)
			{	
				var b = this.state.products.slice(page * 5,((page + 1) * 5))
				groupedProducts.push(b)
			}		
		}
		const options = {
		    items: 1,
		};		 
		const events = {
		    onChanged: function(event) {}
		};
		if(groupedProducts.length == 0)
		{
			return <h6 className="pt-100">No Products</h6>
		}
		else
		{
			return(
				<OwlCarousel ref="car" options={options} events={events} >
					{
						groupedProducts.map((groupedProduct, index)=> (
							<div className="double-pro" key={"g-p-"+index}>
								{
									groupedProduct.map((p, i) => (
										<ProductList key={p.id} product={p} outerClass="col-md-3" onSideBarButtonClick={this.getSideBarButtonClick} />
									))
								}
							</div>
						))
					}
				</OwlCarousel>	
			)
		}	
	}

	render() {
        return (
			<ManageProductContext.Provider value={{
				categories: this.state.categories,
				subCategories: this.state.subCategories,
				products:this.state.products,
				selectedCategoryId:this.state.selectedCategoryId,
				selectedSubCategoryId:this.state.selectedSubCategoryId,
				productForm: this.state.productForm,
				selectedProduct: this.state.selectedProduct}}
			>
			<Header />
			<CategorySearch handleCategoryChange={(value)=>this.handleCategoryChange(value)}/>
			<SubCategorySearch handleSubCategoryChange={(value) => this.handleSubCategoryChange(value)} />
			<div className="new-products pt-60 pb-30">
	            <div className="container">
	               	<div className="row">
	               		<div className="col-xl-5 col-lg-4 order-2">
                    		<div className="side-product-list">
					            <div className="group-title">
					                <h2>Top Products</h2>
					                <button className="prev" onClick={() => this.refs.car.prev()}> <i className="fa fa-angle-left"></i> </button>
									<button className="next" onClick={() => this.refs.car.next()}> <i className="fa fa-angle-right"></i> </button>
								</div>   
								<div className="slider-right-content">
									{ this._renderProducts() }
								</div>     
                    		</div>
                    	</div>
	               		<div className="col-xl-7 col-lg-8  order-lg-2">									
							<ManageProductForm handleInputChange={(value,name) => this.handleInputChange(value,name)} />
						</div>
	               	</div>
               	</div>
            </div>
			<Footer />
			</ManageProductContext.Provider>
        );
    }
}
export default ManageProducts

