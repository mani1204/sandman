import React from 'react'
import PaymentImage from "packs/images/payment-image.png"
class Footer extends React.Component
{
	render()
	{
		return (
			<React.Fragment>
				<footer className="off-white-bg">
		            <div className="footer-top pt-50 pb-60">
		                <div className="container">
		                    <div className="row">
		                        <div className="col-lg-6 mr-auto ml-auto">
		                            <div className="newsletter text-center">
		                                <div className="main-news-desc">
		                                     <div className="news-desc">
		                                         <h3>Sign Up For Newsletters</h3>
		                                         <p>Get e-mail updates about our latest shop and special offers.</p>
		                                     </div>
		                                </div>
		                                <div className="newsletter-box">
		                                    <form action="http://demo.devitems.com/jantrik-v1/index.html#">
		                                        <input className="subscribe" placeholder="Enter your email address" name="email" id="subscribe" type="text"/>
		                                        <button type="submit" className="submit">subscribe</button>
		                                    </form>
		                                </div>
		                             </div>                            
		                        </div>
		                    </div>                    
		                    <div className="row">
		                        <div className="col-lg-4  col-md-7 col-sm-6">
		                            <div className="single-footer">
		                                <h3>Contact us</h3>
		                                <div className="footer-content">
		                                    <div className="loc-address">
		                                        <span><i className="fa fa-map-marker"></i>184 Main Rd E, St Albans VIC 3021, Australia.</span>
		                                        <span><i className="fa fa-envelope-o"></i>Mail Us : yourinfo@example.com</span>
		                                        <span><i className="fa fa-phone"></i>Phone: + 00 254 254565 54</span>
		                                    </div>
		                                    <div className="payment-mth">
		                                    	<a href="http://demo.devitems.com/jantrik-v1/index.html#">
	                                    			<img className="img" src={PaymentImage}/>
	                                    		</a>
		                                    </div>
		                                </div>
		                            </div>
		                        </div>
		                        <div className="col-lg-2  col-md-5 col-sm-6 footer-full">
		                            <div className="single-footer">
		                                <h3 className="footer-title">Information</h3>
		                                <div className="footer-content">
		                                    <ul className="footer-list">
		                                        <li><a href="http://demo.devitems.com/jantrik-v1/index.html#">Site Map</a></li>
		                                        <li><a href="http://demo.devitems.com/jantrik-v1/index.html#">Specials</a></li>
		                                        <li><a href="http://demo.devitems.com/jantrik-v1/index.html#">Jobs</a></li>
		                                        <li><a href="http://demo.devitems.com/jantrik-v1/index.html#">Delivery Information</a></li>
		                                        <li><a href="http://demo.devitems.com/jantrik-v1/index.html#">Order History</a></li>
		                                        <li><a href="http://demo.devitems.com/jantrik-v1/index.html#">Privacy Policy</a></li>
		                                    </ul>
		                                </div>
		                            </div>
		                        </div>
		                        <div className="col-lg-2  col-md-4 col-md-4 col-sm-6 footer-full">
		                            <div className="single-footer">
		                                <h3 className="footer-title">My Account</h3>
		                                <div className="footer-content">
		                                    <ul className="footer-list">
		                                        <li><a href="http://demo.devitems.com/jantrik-v1/account.html">My account</a></li>
		                                        <li><a href="http://demo.devitems.com/jantrik-v1/index.html#">Checkout</a></li>
		                                        <li><a href="http://demo.devitems.com/jantrik-v1/index.html#">Login</a></li>
		                                        <li><a href="http://demo.devitems.com/jantrik-v1/index.html#">address</a></li>
		                                        <li><a href="http://demo.devitems.com/jantrik-v1/index.html#">Order status</a></li>
		                                        <li><a href="http://demo.devitems.com/jantrik-v1/index.html#">Site Map</a></li>
		                                    </ul>
		                                </div>
		                            </div>
		                        </div>
		                        <div className="col-lg-2 col-md-4 col-sm-6 footer-full">
		                            <div className="single-footer">
		                                <h3 className="footer-title">customer service</h3>
		                                <div className="footer-content">
		                                    <ul className="footer-list">
		                                        <li><a href="http://demo.devitems.com/jantrik-v1/account.html">My account</a></li>
		                                        <li><a href="http://demo.devitems.com/jantrik-v1/index.html#">New</a></li>
		                                        <li><a href="http://demo.devitems.com/jantrik-v1/index.html#">Gift Cards</a></li>
		                                        <li><a href="http://demo.devitems.com/jantrik-v1/index.html#">Return Policy</a></li>
		                                        <li><a href="http://demo.devitems.com/jantrik-v1/index.html#">Your Orders</a></li>
		                                        <li><a href="http://demo.devitems.com/jantrik-v1/index.html#">Subway</a></li>
		                                    </ul>
		                                </div>
		                            </div>
		                        </div>
		                        <div className="col-lg-2 col-md-4 col-sm-6 footer-full">
		                            <div className="single-footer">
		                                <h3 className="footer-title">Let Us Help You</h3>
		                                <div className="footer-content">
		                                    <ul className="footer-list">
		                                        <li><a href="http://demo.devitems.com/jantrik-v1/index.html#">Your Account</a></li>
		                                        <li><a href="http://demo.devitems.com/jantrik-v1/index.html#">Your Orders</a></li>
		                                        <li><a href="http://demo.devitems.com/jantrik-v1/index.html#">Shipping</a></li>
		                                        <li><a href="http://demo.devitems.com/jantrik-v1/index.html#">Amazon Prime</a></li>
		                                        <li><a href="http://demo.devitems.com/jantrik-v1/index.html#">Replacements</a></li>
		                                        <li><a href="http://demo.devitems.com/jantrik-v1/index.html#">Manage </a></li>
		                                    </ul>
		                                </div>
		                            </div>
		                        </div>
		                    </div>
		                </div>
		            </div>
		            <div className="footer-bottom off-white-bg2">
		                <div className="container">
		                    <div className="footer-bottom-content">
		                        <p className="copy-right-text">Copyright © <a href="http://demo.devitems.com/jantrik-v1/index.html#">Jantrik</a> All Rights Reserved.</p>
		                        <div className="footer-social-content">
		                            <ul className="social-content-list">
		                                <li><a href="http://demo.devitems.com/jantrik-v1/index.html#"><i className="fab fa-twitter"></i></a></li>
		                                <li><a href="http://demo.devitems.com/jantrik-v1/index.html#"><i className="fa fa-wifi"></i></a></li>
		                                <li><a href="http://demo.devitems.com/jantrik-v1/index.html#"><i className="fab fa-google-plus"></i></a></li>
		                                <li><a href="http://demo.devitems.com/jantrik-v1/index.html#"><i className="fab fa-facebook"></i></a></li>
		                                <li><a href="http://demo.devitems.com/jantrik-v1/index.html#"><i className="fab fa-youtube"></i></a></li>
		                            </ul>
		                        </div>
		                    </div>
		                </div>
		            </div>
		        </footer>
			</React.Fragment>
		);
	}
}

export default Footer