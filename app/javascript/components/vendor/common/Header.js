import React from "react"
import PropTypes from "prop-types"
import CallIcon from "packs/images/call.png"
import Logo from "packs/images/logo.png"
class Header extends React.Component {
	componentDidMount()
	{
		jQuery('.mobile-menu nav').meanmenu({
		    meanScreenWidth: "991",
		    meanMenuContainer: "div.mobile-menu"
		});
	}
	
  	render () {
	    return (
	      	<React.Fragment>
	     		<header>
		            <div className="header-top">
		                <div className="container">
		                    <div className="row">
		                        <div className="col-lg-4 col-md-12 d-center">
		                            <div className="header-top-left">
		                                <img src={CallIcon}/>
		                                Call Us : +11 222 3333
		                            </div>                        
		                        </div>
		                    </div>
		                </div>
		            </div>

		            <div className="header-bottom header-sticky">
		                <div className="container">
		                    <div className="row">
		                        <div className="col-xl-3 col-lg-2 col-sm-5 col-5">
		                            <div className="logo">
		                                <a href="/">
		                                	<img src={Logo} />
		                                </a>
		                            </div>
		                        </div>
		                        <div className="col-xl-8 col-lg-7 d-none d-lg-block">
		                            <div className="middle-menu pull-right">
		                                <nav>
		                                    <ul className="middle-menu-list">
		                                        <li>
		                                        	<a href="/vendors/my_products">
		                                        		<span>Products </span>
		                                        		<i className="fal fa-angle-down fa-lg"></i>
		                                        	</a>
		                                            <ul className="ht-dropdown dropdown-style-two">
		                                                <li><a href="/vendors/manage_products">Manage Products</a></li>
		                                                <li><a href="/vendors/my_products">My Products</a></li>
		                                            </ul>
		                                        </li>
		                                    </ul>
		                                </nav>
		                            </div>
		                        </div>
		                        <div className="col-lg-1 col-sm-7 col-7">
		                            <div className="cart-box text-right">
		                                <ul>
		                                    <li>
		                                    	<a href="/vendors/logout" data-method="delete">
		                                    		<i className="fas fa-sign-in-alt"></i>
		                                    	</a>
		                                    </li>              
		                                </ul>
		                            </div>
		                        </div>
		                        <div className="col-sm-12 d-lg-none">
		                            <div className="mobile-menu">
		                                <nav>
		                                    <ul>
		                                        <li><a href="/vendors/my_products">Products</a>
		                                            <ul>
		                                                <li><a href="/vendors/manage_products">Manage Products</a></li>
		                                                <li><a href="/vendors/my_products">My Products</a></li>
		                                            </ul>
		                                        </li>
		                                    </ul>
		                                </nav>
		                            </div>
		                        </div>
		                    </div>
		                </div>
		            </div>
		        </header>
	      	</React.Fragment>
	    );
  	}
}

Header.propTypes = {
  greeting: PropTypes.string
};
export default Header

