import React from "react"
import Header from "../common/Header"
import ResetPasswordIndex from "../partials/vendor/reset-password/Index";
import Footer from "../common/Footer"
import Navigation from '../common/Navigation'
class ResetPassword extends React.Component {
	constructor(props) {
		super(props)
	}

	render() {
		return (
			<React.Fragment>
				<div className="wrapper homepage">
					<Header />
					<Navigation>
						<ul>
							<li><a href="/">Home</a></li>
							<li className="active"><a href="#">Forgot Password</a></li>
						</ul>
					</Navigation>
					<ResetPasswordIndex />
					<Footer />
				</div>
			</React.Fragment>
		);
	}
}
export default ResetPassword

