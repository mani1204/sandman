import React from "react"
import Header from "../common/Header.jsx"
import Footer from "../common/Footer"
import VendorLogin from "../../services/vendor/Login"
import {csrfToken} from "../../utils/dom"
class Login extends React.Component
{
	constructor(props)
	{
		super(props)

		this.alert = this.alert.bind(this)
		this.notice = this.notice.bind(this)
	}

	alert()
	{
		if(this.props.alert)
		{
			return (
				<div className="alert alert-danger" role="alert">
        			{this.props.alert}
        		</div>
			)
		}
		else
		{
			return ''
		}
	}

	notice()
	{
		if(this.props.notice)
		{
			return (
				<div className="alert alert-secondary" role="alert">
        			{this.props.notice}
        		</div>
			)
		}
		else
		{
			return ''
		}
	}


	render () {
        return (
          	<div className="wrapper homepage">
              	<Header/>
				<div className="breadcrumb-area ptb-60 ptb-sm-30">
				    <div className="container">
				        <div className="breadcrumb">
				            <ul>
				                <li><a href="index.html">Home</a></li>
				                <li className="active"><a href="login.html">Login</a></li>
				            </ul>
				        </div>
				    </div>
				</div>

				<div className="log-in pb-60">
		            <div className="container">
		                <div className="row">
		                    <div className="col-sm-6">
		                        <div className="well">
		                            <div className="new-customer">
		                                <h3>NEW VENDOR</h3>
		                                <p className="mtb-10"><strong>Register</strong></p>
		                                <p>By creating an account you will be able to shop faster, be up to date on an order's status, and keep track of the orders you have previously made</p>
		                                <a className="customer-btn" href="register">continue</a>
		                            </div>
		                        </div>
		                    </div>
		                    <div className="col-sm-6">
			                    { this.alert() }
			                    { this.notice() }
		                        <div className="well">
		                            <div className="return-customer">
		                                <h3 className="mb-10">RETURNING Vendor</h3>
		                                <p className="mb-10"><strong>I am a returning vendor</strong></p>
		                                <form action="/vendors/login" method="post">
		                                	<input type="hidden" name="authenticity_token" value={csrfToken} />
		                                    <div className="form-group">
		                                        <input type="text" name="user[phone]" placeholder="Enter your Phone Number here..." id="input-phone" className="form-control"/>
		                                    </div>
		                                    <div className="form-group">
		                                        <input type="password" name="user[password]" placeholder="Password" id="input-password" className="form-control"/>
		                                    </div>
		                                    <p className="lost-password"><a href="/vendors/reset_password">Forgot password?</a></p>
		                                    <input type="submit" value="Login" className="return-customer-btn"/>
		                                </form>
		                            </div>
		                        </div>
		                    </div>
	                	</div>
	            	</div>
	       		</div>
      			<Footer/>
          	</div>
        );
    }
}


export default Login

