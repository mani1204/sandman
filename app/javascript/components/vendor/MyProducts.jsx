import React from "react"
import Header from "./common/Header"
import Footer from "./common/Footer"
class MyProducts extends React.Component
{
	constructor(props)
	{
		super(props)
	}

	render () {
        return (
        	<React.Fragment>
	          	<Header />
	          	My Products
	          	<Footer />
          	</React.Fragment>
        );
    }
}

export default MyProducts

