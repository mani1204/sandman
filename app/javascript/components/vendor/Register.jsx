import React from "react";
import Header from "../common/Header.jsx";
import Footer from "../common/Footer";
import VendorLogin from "../../services/vendor/Login";
import {csrfToken} from "../../utils/dom";
import RegisterPage from "components/partials/vendor/registration/RegisterPage";
import Navigation from "../common/Navigation"


class Register extends React.Component
{
  render () {
    return (
      <div className="wrapper homepage">
        <Header/>
        <Navigation> 
          <ul>
            <li><a href="/">Home</a></li>
            <li className="active"><a href="#">Register</a></li>
          </ul>
        </Navigation>
        <RegisterPage />				
        <Footer/>
      </div>
    );
  }
}


export default Register

