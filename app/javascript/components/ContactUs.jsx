import React from "react"
import PropTypes from "prop-types"
import CustomerContext from "utils/context/CustomerContext"
import Header from "./common/Header"
import Footer from "./common/Footer"
import Navigation from './common/Navigation'
import ContactForm from './partials/contact/ContactForm'

class ContactUs extends React.Component {
	render() {
		return (
			<CustomerContext.Provider value={{user: this.props.currentUser}}>
				<React.Fragment>
					<div className="wrapper homepage">
						<Header />
						<Navigation>
							<ul>
								<li><a href="index.html">Home</a></li>
								<li className="active"><a href="contact.html">Contact</a></li>
							</ul>
						</Navigation>>
						<ContactForm />
						<Footer />
					</div>
				</React.Fragment>
			</CustomerContext.Provider>
		)
	}
}
ContactUs.propTypes = {
    currentUser: PropTypes.object
};
export default ContactUs