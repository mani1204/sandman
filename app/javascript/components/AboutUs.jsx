import React from "react"
import PropTypes from "prop-types"
import Header from "./common/Header"
import Footer from "./common/Footer"
import Navigation from './common/Navigation'
import AboutUsPage from './partials/about-us/About-Us'
import CustomerContext from "utils/context/CustomerContext"

class AboutUs extends React.Component {
    render() {
        return (
            <CustomerContext.Provider value={{user: this.props.currentUser}}>
                <React.Fragment>
                    <div className="wrapper homepage">
                        <Header />
                        <Navigation>
                            <ul>
                                <li><a href="/">Home</a></li>
                                <li className="active"><a href="#">AboutUs</a></li>
                            </ul>
                        </Navigation>
                        <AboutUsPage/>
                        <Footer/>
                    </div>
                </React.Fragment>
            </CustomerContext.Provider>
        )
    }
}
AboutUs.propTypes = {
    currentUser: PropTypes.object
};

export default AboutUs