module Customer::Account
	def self.create_account(mobile)
		user = User.where(phone: mobile).first
		user = User.create!(country_code: "+91", phone: mobile) if user.nil?
		user.create_customer_profile() if user.customer_profile.nil?
	end
end