module SMS::MSG91
	def self.get_otp(data)
		conn = self.conn
		response = conn.post('api/sendotp.php', { 
			authkey: ENV['MSG91_AUTH_KEY'], 
			message: 'Your OTP is ##OTP##',
			sender:	 'SN6782',
			mobile:	 '91' + data[:mobile]
		})
		puts response
		response = JSON.parse(response.body)
	end

	def self.resend_otp(data)
		conn = self.conn
		response = conn.post('api/retryotp.php', { 
			authkey: 	ENV['MSG91_AUTH_KEY'], 
			mobile: 	'91' + data[:mobile],
			retrytype: 	'text'
		})

		response = JSON.parse(response.body)
	end

	def self.verify_otp(data)
		conn = self.conn
		response = conn.post('api/verifyRequestOTP.php', { 
			authkey: ENV['MSG91_AUTH_KEY'], 
			mobile: '91' + data[:mobile],
			otp: data[:otp]
		})
		response = JSON.parse(response.body)
	end

	def self.conn
		Faraday.new(:url => ENV['MSG91_URL'])
	end
end