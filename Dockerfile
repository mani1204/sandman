FROM ruby:2.6.2-alpine

RUN apk add --no-cache --update build-base linux-headers git postgresql-dev nodejs tzdata imagemagick yarn curl supervisor

WORKDIR /sandman
ADD Gemfile Gemfile.lock /sandman/
RUN bundle install